#include "splines.h"

// Source : http://www.polytech-lille.fr/cours-algos-calcul-scientifique/progrcit.html

/*
 Les paramètres d'appel de cette fonction sont :

    a : un tableau carré (la matrice du système) ;
    b : un tableau unidimensionnel (le second membre du système) ;
    n : l'ordre de la matrice. 

Elle renvoie la valeur 0 si le calcul a rencontré un pivot nul et la valeur 1 sinon ; 
quand la valeur renvoyée est 1, la solution du système se trouve dans le tableau b.

La constante entière NMAX est égale à la dimension maximale de la matrice (+1).
*/

int sl_gauss_tridiag(double a[NMAX][NMAX],double b[NMAX],int n)
{
 int i;
 double den;
 double w[NMAX],y[NMAX];
 if (a[1][1]==0) return(0);
 w[1]=a[1][2]/a[1][1];
 y[1]=b[1]/a[1][1];
 for(i=2;i<=n;i++)
 {
  den=a[i][i]-a[i][i-1]*w[i-1];
  if (den==0) return(0);
  if (i<n) w[i]=a[i][i+1]/den;
  y[i]=(b[i]-a[i][i-1]*y[i-1])/den;
 }
 b[n]=y[n];
 for(i=n-1;i>=1;i--)
 b[i]=y[i]-b[i+1]*w[i];
 return(1);
}

/*
 Les paramètres d'appel de cette fonction sont :

    n : le nombre de points d'interpolation moins 1 (c'est-à-dire l'indice du dernier) ;
    x : un tableau contenant les abscisses d'interpolation ;
    f : un tableau contenant les valeurs de la fonction f(x) aux points d'interpolation ;
    b : un tableau qui renvoie les coefficients de la spline. 

 Elle calcule dans le tableau b les coefficients de la spline cubique, associée à la fonction f(x), 
 dont les valeurs sont fournies par le paramètre f, pour les n+1 valeurs distinctes de la variable 
 données par le paramètre x.
*/

int it_coef_spl3(int n,double x[NMAX],double f[NMAX],double b[NMAX])
{
 int i,j;
 double h[NMAX],a[NMAX][NMAX];
 for(i=1;i<=n;i++)
 {
  h[i]=x[i]-x[i-1];
  for(j=1;j<=n;j++)
  a[i][j]=0;
 }
 a[1][1]=(h[1]+h[2])/3;
 a[1][2]=h[2]/6;
 for(i=2;i<n-1;i++)
 {
  a[i][i-1]=h[i]/6;
  a[i][i]=(h[i]+h[i+1])/3;
  a[i][i+1]=h[i+1]/6;
 }
 a[n-1][n-2]=h[n-1]/6;
 a[n-1][n-1]=(h[n-1]+h[n])/3;
 for(i=1;i<n;i++)
 b[i]=(f[i+1]-f[i])/h[i+1]-(f[i]-f[i-1])/h[i];
 return(sl_gauss_tridiag(a,b,n-1));
}

/*
 Les paramètres d'appel de cette fonction sont :

    n : le nombre de points d'interpolation moins 1 (c'est-à-dire l'indice du dernier) ;
    x : un tableau contenant les abscisses d'interpolation ;
    f : un tableau contenant les valeurs de la fonction f(x) aux points d'interpolation ;
    m : un tableau contenant les coefficients de la spline ;
    alpha : la valeur de la variable. 

Elle renvoie, pour la valeur donnée de la variable, la valeur de la spline cubique d'interpolation .
*/

double it_spline_3(int n,double x[NMAX],double f[NMAX],double m[NMAX],double alpha)
{
 int i;
 double p;
 double h[NMAX];
 for(i=1;i<=n;i++)
 h[i]=x[i]-x[i-1];
 for(i=1;i<=n;i++)
 if((alpha>=x[i-1])&&(alpha<=x[i]))
 {
  p=m[i-1]*(x[i]-alpha)*(pow((x[i]-alpha),2)-pow(h[i],2))/(6*h[i]);
  p+=m[i]*(alpha-x[i-1])*(pow((alpha-x[i-1]),2)-pow(h[i],2))/(6*h[i]);
  p+=f[i-1]*(x[i]-alpha)/h[i]+f[i]*(alpha-x[i-1])/h[i];
 }
 return(p);
}
