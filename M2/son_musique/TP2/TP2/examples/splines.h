#ifndef SPLINES
#define SPLINES

#include <math.h>
#define NMAX 10
int sl_gauss_tridiag(double a[NMAX][NMAX],double b[NMAX],int n);
int it_coef_spl3(int n,double x[NMAX],double f[NMAX],double b[NMAX]);
double it_spline_3(int n,double x[NMAX],double f[NMAX],double m[NMAX],double alpha);

#endif
