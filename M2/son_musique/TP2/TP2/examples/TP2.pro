#-------------------------------------------------
#
# Project created by QtCreator 2014-09-22T15:51:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = TP2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    ../3d.cpp \
    ../channel_groups.cpp \
    ../common.cpp \
    ../common_platform.cpp \
    ../dsp_custom.cpp \
    ../dsp_effect_per_speaker.cpp \
    ../effects.cpp \
    ../gapless_playback.cpp \
    ../generate_tone.cpp \
    ../granular_synth.cpp \
    ../load_from_memory.cpp \
    ../multiple_speaker.cpp \
    ../multiple_system.cpp \
    ../net_stream.cpp \
    ../play_sound.cpp \
    ../play_stream.cpp \
    ../record.cpp \
    ../splines.cpp \
    ../tp2.cpp \
    ../user_created_sound.cpp \
    3d.cpp \
    channel_groups.cpp \
    common.cpp \
    common_platform.cpp \
    dsp_custom.cpp \
    dsp_effect_per_speaker.cpp \
    effects.cpp \
    gapless_playback.cpp \
    generate_tone.cpp \
    granular_synth.cpp \
    load_from_memory.cpp \
    main.cpp \
    multiple_speaker.cpp \
    multiple_system.cpp \
    net_stream.cpp \
    play_sound.cpp \
    play_stream.cpp \
    record.cpp \
    splines.cpp \
    tp2.cpp \
    user_created_sound.cpp

HEADERS += \
    ../common.h \
    ../common_platform.h \
    ../splines.h \
    common.h \
    common_platform.h \
    splines.h
