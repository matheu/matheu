/*===============================================================================================
 Ce fichier est issu du programme "User Created Sound Example" de l'API FMODEx (c)
 Son but est d'effectuer diverses synthèses sonores en programmation de bas niveau.
===============================================================================================*/
#include "fmod.hpp"
#include "fmod_errors.h"
#include "common.h"
#include "splines.h"
#include <math.h>
#include <time.h>
#include <iostream>
#define PI 3.1415926535

using namespace std;

/* CONSTANTES ET VARIABLES GLOBALES */

enum modes_lecture {LECT_FICHIER, SYNTH_SONORE} ;
enum modes_lecture mode_lecture = SYNTH_SONORE ;

enum amplitudes {AMP_GLOBALE,AMP_ADSR} ;
float ampGauche=0.5, ampDroite=0.5 ;
float adsr (float x);
float Attack=0.03, Decay=0.1, Duree=4, Sustain=0.8, Release=0.5 ;
enum amplitudes mode_amp = AMP_GLOBALE ;

enum frequences {FREQ_MANUELLE,FREQ_ENVELOPPE_LIN,FREQ_ENVELOPPE_S,FREQ_ENVELOPPE_LOG,FREQ_ENVELOPPE_SPLINE} ;
float freq = 440.0f ;
#define numFrequences 4
double TempsFrequences[2][numFrequences]={{0.0f,1.0f,2.0f,3.0f},{200.0f,170.0f,300.0f,200.0f}};
float frequence (float t) ;
enum frequences mode_freq = FREQ_MANUELLE ;

enum timbres {PUR,DISTORDU,CARRE,TRIANGLE,ADDITIF,BRUIT,KARPLUS} ;
float distorsion = 10 ;
#define nbSin 9
float harmo[nbSin] = {1. , 0 , 1./3, 0, 1./5, 0, 1./7, 0, 1./9} ;
#define nbEch 500
float Circle[nbEch] ; // buffer circulaire pour Karplus-Strong
enum timbres mode_timbre = BRUIT ;

FMOD_DSP_TYPE mode_dsp = FMOD_DSP_TYPE_LOWPASS ;
float freqDSP = 1000.0f ;
float largeurBande=1.0f;
float resonance = 4.0f;

/* CALCULS INTERMEDIAIRES */

float amplitude (float x)
{
	float resultat ;
	switch (mode_amp) {
		case AMP_ADSR :
		{
			resultat = (x < Attack ? x/Attack :
             (x < Attack + Decay ? 1+(Sustain-1)*(x-Attack)/Decay :
                (x < Attack + Decay + Duree ? Sustain :
                   (x < Attack + Decay + Duree + Release ? Sustain - Sustain*(x-(Attack+Decay+Duree))/Release :
                      0))));
            break ;
        }
        case AMP_GLOBALE :
        {
			resultat = 1 ;
			break ;
		}
	}
	return resultat ;
}

/* Solution dysfonctionnelle : augmenter continûment la fréquence 
 * fait apparaître des fréquences plus élevées. Changer par micro-paliers ? ... */

float frequence (float t) 
{
	float resultat ;

	switch (mode_freq)
	{
		case FREQ_ENVELOPPE_S :
		{
			if (t <= TempsFrequences[0][0]) 
				resultat = TempsFrequences[1][0] ;
			else if (t >= TempsFrequences[0][numFrequences-1])
				resultat = TempsFrequences[1][numFrequences-1] ;
			else 
			{
				static int i=1 ;
				float f, t1, t2, f1, f2 ;
				float ce = 5.0f ;
				while (TempsFrequences[0][i]<t)
					i++ ;
				t1=TempsFrequences[0][i-1] ;
				t2=TempsFrequences[0][i] ; 
				f1=TempsFrequences[1][i-1] ;
				f2=TempsFrequences[1][i] ;
				resultat = f1 + (f2-f1) / (1 + ce * exp(-2 * log(ce) * (t-t1) / (t2-t1)) ) ;
			}
			break ;
		}
		case FREQ_ENVELOPPE_LIN :
		{
			if (t <= TempsFrequences[0][0]) 
				resultat = TempsFrequences[1][0] ;
			else if (t >= TempsFrequences[0][numFrequences-1])
				resultat = TempsFrequences[1][numFrequences-1] ;
			else 
			{
				static int i=1 ;
				float f, t1, t2, f1, f2 ;
				float ce = 5.0f ;
				while (TempsFrequences[0][i]<t)
					i++ ;
				t1=TempsFrequences[0][i-1] ;
				t2=TempsFrequences[0][i] ; 
				f1=TempsFrequences[1][i-1] ;
				f2=TempsFrequences[1][i] ;
				resultat = f1 + (f2-f1) * (t-t1) / (t2-t1) ;
			}
			break ;
		}
		case FREQ_ENVELOPPE_LOG :
		{
			if (t <= TempsFrequences[0][0]) 
				resultat = TempsFrequences[1][0] ;
			else if (t >= TempsFrequences[0][numFrequences-1])
				resultat = TempsFrequences[1][numFrequences-1] ;
			else 
			{
				static int i=1 ;
				float f, t1, t2, f1, f2 ;
				float ce = 5.0f ;
				while (TempsFrequences[0][i]<t)
					i++ ;
				t1=TempsFrequences[0][i-1] ;
				t2=TempsFrequences[0][i] ; 
				f1=log(TempsFrequences[1][i-1]) ;
				f2=log(TempsFrequences[1][i]) ;
				resultat = exp(f1 + (f2-f1) * (t-t1) / (t2-t1)) ;
			}
			break ;
		}
		case FREQ_ENVELOPPE_SPLINE :
		{
			double coefs[NMAX];
			it_coef_spl3(numFrequences-1,TempsFrequences[0],TempsFrequences[1],coefs);
			resultat=it_spline_3(numFrequences-1,TempsFrequences[0],TempsFrequences[1],coefs,t);
			break;
	    }	
		case FREQ_MANUELLE :
		{
			resultat = freq ;
			break;
		}
	}
	return resultat;
}

void initKS ()
{
	// initialisation de l'algorithme de Karplus-Strong
}

float timbre (float t)
{
	float resultat=0 ;
	
        switch (mode_timbre)
            {
            case PUR :
            {
              resultat = sin(2*PI*freq*t) ;
              break;
            }
            case CARRE :
            {
              resultat = ((sin(2*PI*freq*t)>0) ? (1): (-1)) ;
              break;
            }
            case TRIANGLE :
            {
              resultat = (4.0f*fabs(freq*t-floor(freq*t))-1.0f);
              break;
            }
            case ADDITIF :
            {
            float somme=0.0f;
			  resultat = 0.0f ;
              for(int i=0;i<nbSin;i++)
              {
                  resultat+=harmo[i]*sin(2*PI*(1+i)*freq*t);
                  somme +=harmo[i];
              }
              resultat/=somme;
			  break ;
            }
            case BRUIT :
            {
                resultat = rand() % 2 -1 ;
				break ;
			}
			case KARPLUS :
			{
				resultat = 0 ;				
				break ;
			}
		}
   return resultat ;
}

FMOD_RESULT F_CALLBACK cb_globale (FMOD_SOUND *sound, void *data, unsigned int datalen)
{
    unsigned int  count;
    static float  t = 0;
    signed short *stereo16bitbuffer = (signed short *)data;

    for (count=0; count<datalen>>2; count++)
    {
        freq = frequence(t) ;
        *stereo16bitbuffer++ = (signed short) 32767.0f * ampGauche * amplitude(t) * timbre(t) ;
        *stereo16bitbuffer++ = (signed short) 32767.0f * ampDroite * amplitude(t) * timbre(t) ;
        t += 1.0f/44100 ;
    }
    return FMOD_OK;
}




FMOD_RESULT F_CALLBACK pcmsetposcallback(FMOD_SOUND *sound, int subsound, unsigned int position, FMOD_TIMEUNIT postype)
{
    return FMOD_OK;
}

int main(int argc, char *argv[])
{

    FMOD::System           *system;
    FMOD::Sound            *sound;
    FMOD::Channel          *channel = 0;
    FMOD::ChannelGroup 		*mastergroup;
    FMOD::DSP          		*monDsp;
    FMOD_RESULT             result;
    FMOD_MODE               mode = FMOD_OPENUSER|FMOD_CREATESTREAM ;
    FMOD_CREATESOUNDEXINFO  createsoundexinfo;
    int                     channels = 2;
    unsigned int            version;
    void                   *extradriverdata = 0;
    
    Common_Init(&extradriverdata);

    /* Initialisation du moteur audio */

    result = FMOD::System_Create(&system);
    ERRCHECK(result);

    result = system->getVersion(&version);
    ERRCHECK(result);

    if (version < FMOD_VERSION)
    {
        Common_Fatal("Error!  You are using an old version of FMOD %08x.  This program requires %08x\n", version, FMOD_VERSION);
        return 0;
    }

    result = system->init(32, FMOD_INIT_NORMAL, 0);
    ERRCHECK(result);

	switch (mode_lecture)
	{
		case LECT_FICHIER :
		{
			// Chargement d'un fichier à jouer
			break ;
		}
		case SYNTH_SONORE :
		{
			memset(&createsoundexinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
			createsoundexinfo.cbsize            = sizeof(FMOD_CREATESOUNDEXINFO);
			createsoundexinfo.decodebuffersize  = 3000;
			createsoundexinfo.length            = 44100 * channels * sizeof(signed short) * 5;
			createsoundexinfo.numchannels       = channels;
			createsoundexinfo.defaultfrequency  = 44100;
			createsoundexinfo.format            = FMOD_SOUND_FORMAT_PCM16;
			createsoundexinfo.pcmsetposcallback = pcmsetposcallback;
			mode |= FMOD_LOOP_NORMAL ;
			createsoundexinfo.pcmreadcallback   = cb_globale;
			result = system->createSound(0, mode, &createsoundexinfo, &sound);
			ERRCHECK(result);

			/* Initialisation des algorithmes */

			if (mode_timbre==BRUIT)
                {    srand(time(NULL));} // initialisation de l'algorithme de synthèse de bruit
			else if (mode_timbre==KARPLUS)
				{} // initialisation de l'algorithme de Karplus-Strong

			break ;
		}
	}
 
    /* Lecture du buffer */

    result = system->playSound(sound, 0, 0, &channel);
    ERRCHECK(result);

	/* Création du DSP */

	switch (mode_dsp)
	{
		case FMOD_DSP_TYPE_LOWPASS :
		{
            result = system->createDSPByType(FMOD_DSP_TYPE_LOWPASS, &monDsp);
            ERRCHECK(result);
            result = monDsp->setParameterFloat(FMOD_DSP_LOWPASS_CUTOFF, freqDSP);
            ERRCHECK(result);
            result = monDsp->setParameterFloat(FMOD_DSP_LOWPASS_RESONANCE, resonance);
            ERRCHECK(result);
			break ;
	    }
		case FMOD_DSP_TYPE_PARAMEQ :
		{
            result = system->createDSPByType(FMOD_DSP_TYPE_PARAMEQ, &monDsp);
            ERRCHECK(result);
            result = monDsp->setParameterFloat(FMOD_DSP_PARAMEQ_CENTER, freqDSP);
            ERRCHECK(result);
            result = monDsp->setParameterFloat(FMOD_DSP_PARAMEQ_BANDWIDTH, resonance);
            ERRCHECK(result);
            break ;
	    }
	}
    /*
        Connexion du DSP.
    */

    result = monDsp->setBypass(true);
    ERRCHECK(result);

    result = system->getMasterChannelGroup(&mastergroup);
    ERRCHECK(result);

    result = mastergroup->addDSP(0, monDsp);
    ERRCHECK(result);

    /* Boucle principale */

    do
    {
        /* Lecture des commandes de l'utilisateur pendant la lecture 
           et définition des changements à appliquer */

        bool bypass;

        Common_Update();

        result = monDsp->getBypass(&bypass);
        ERRCHECK(result);

        if (Common_BtnPress(BTN_MORE))
            {
                bool paused;
                channel->getPaused(&paused);
                channel->setPaused(!paused);

            }
		else if (Common_BtnPress(BTN_ACTION1))
            {
                ampGauche *= 2 ;
                ampDroite *= 2 ;
            }
		else if (Common_BtnPress(BTN_ACTION2))
            {
                ampGauche /= 2 ;
                ampDroite /= 2 ;
            }
		else if (Common_BtnPress(BTN_ACTION3))
            {
                freq /=1.05946 ;
            }
		else if (Common_BtnPress(BTN_ACTION4))
            {
                freq *=1.05946 ;
            }
		else if (Common_BtnPress(BTN_ACTION5))
            {
                distorsion *=2 ;
            }
		else if (Common_BtnPress(BTN_ACTION6))
            {
                distorsion /=2 ;
            }
		else if (Common_BtnPress(BTN_ACTION7))
            {
                ampGauche = (ampGauche <= 0.5 ? ampGauche* 2 : ampGauche) ;
                ampDroite /= 2 ;
            }
		else if (Common_BtnPress(BTN_ACTION8))
            {
                ampDroite = (ampDroite <= 0.5 ? ampDroite* 2 : ampDroite) ;
                ampGauche /= 2 ;
            }
		else if (Common_BtnPress(BTN_ACTION11))
            {
                bypass = !bypass;

                result = monDsp->setBypass(bypass);
                ERRCHECK(result);
            }
		else if (Common_BtnPress(BTN_ACTION12))
            {
				// Diminution de la fréquence (filtre passe-bande)

                switch (mode_dsp)
                {
                    case FMOD_DSP_TYPE_LOWPASS :
                    {
                        freqDSP/=1.05946;
                        result = monDsp->setParameterFloat(FMOD_DSP_LOWPASS_CUTOFF, freqDSP);
                        ERRCHECK(result);
                        break ;
                    }
                    case FMOD_DSP_TYPE_PARAMEQ :
                    {
                        freqDSP/=1.05946;
                        result = monDsp->setParameterFloat(FMOD_DSP_PARAMEQ_CENTER, freqDSP);
                        ERRCHECK(result);
                        break ;
                    }
                }
            }
		else if (Common_BtnPress(BTN_ACTION13))
            {
				// Augmentation de la fréquence (filtre passe-bande)

                switch (mode_dsp)
                {
                    case FMOD_DSP_TYPE_LOWPASS :
                    {
                        freqDSP*=1.05946;
                        result = monDsp->setParameterFloat(FMOD_DSP_LOWPASS_CUTOFF, freqDSP);
                        ERRCHECK(result);
                        break ;
                    }
                    case FMOD_DSP_TYPE_PARAMEQ :
                    {
                        freqDSP*=1.05946;
                        result = monDsp->setParameterFloat(FMOD_DSP_PARAMEQ_CENTER, freqDSP);
                        ERRCHECK(result);
                        break ;
                    }
                }

           }
		else if (Common_BtnPress(BTN_ACTION14))
            {
				// Diminution de la largeur de bande (filtre passe-bande)
                switch (mode_dsp)
                {
                    case FMOD_DSP_TYPE_LOWPASS :
                    {
                        resonance-=0.5;
                        resonance = (resonance<1?1:resonance);
                        result = monDsp->setParameterFloat(FMOD_DSP_LOWPASS_RESONANCE, resonance);
                        ERRCHECK(result);
                        break ;
                    }
                    case FMOD_DSP_TYPE_PARAMEQ :
                    {
                        resonance-=0.5;
                        resonance = (resonance<0.2?0.2:resonance);
                        result = monDsp->setParameterFloat(FMOD_DSP_PARAMEQ_BANDWIDTH, resonance);
                        ERRCHECK(result);
                        break ;
                    }
                }
            }
		else if (Common_BtnPress(BTN_ACTION15))
            {
				// Augmentation de la largeur de bande (filtre passe-bande)
                switch (mode_dsp)
                {
                    case FMOD_DSP_TYPE_LOWPASS :
                    {
                        resonance+=0.5;
                        resonance = (resonance<10?10:resonance);
                        result = monDsp->setParameterFloat(FMOD_DSP_LOWPASS_RESONANCE, resonance);
                        ERRCHECK(result);
                        break ;
                    }
                    case FMOD_DSP_TYPE_PARAMEQ :
                    {
                        resonance+=0.5;
                        resonance = (resonance<5?5:resonance);
                        result = monDsp->setParameterFloat(FMOD_DSP_PARAMEQ_BANDWIDTH, resonance);
                        ERRCHECK(result);
                        break ;
                    }
                }
            }

            
	/* Application des changements */

        system->update();

	/* Récupération et affichage de l'état de la lecture (en pause, arrêté, etc.) */

            Common_Draw("Amplitude : gauche %.2f   droite : %.2f",ampGauche, ampDroite);
            Common_Draw("Fréquence : %.2f",freq);
            Common_Draw("Distorsion : %.2f",distorsion);
			Common_Draw("%s=pause   %s/%s=Vol-/+   %s/%s=Freq-/+",
				Common_BtnStr(BTN_MORE),Common_BtnStr(BTN_ACTION1),Common_BtnStr(BTN_ACTION2),Common_BtnStr(BTN_ACTION3),Common_BtnStr(BTN_ACTION4));
			Common_Draw("%s/%s=distorsion-/+   %s/%s=gauche/droite",
				Common_BtnStr(BTN_ACTION5),Common_BtnStr(BTN_ACTION6),Common_BtnStr(BTN_ACTION7),Common_BtnStr(BTN_ACTION8));
			Common_Draw("%s=DSPOnOff %s/%s=freqDSP-/+  %s/%s=largeur-/+",
				Common_BtnStr(BTN_ACTION11),Common_BtnStr(BTN_ACTION12),Common_BtnStr(BTN_ACTION13),Common_BtnStr(BTN_ACTION14),Common_BtnStr(BTN_ACTION15));
			Common_Draw("%s=quitter",Common_BtnStr(BTN_QUIT));

        if (channel)
        {
            unsigned int ms;
            unsigned int lenms;
            bool         playing;
            bool         paused;

            result = channel->isPlaying(&playing);
            if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            {
                ERRCHECK(result);
            }

            result = channel->getPaused(&paused);
            if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            {
                ERRCHECK(result);
            }

            result = channel->getPosition(&ms, FMOD_TIMEUNIT_MS);
            if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            {
                ERRCHECK(result);
            }

            result = sound->getLength(&lenms, FMOD_TIMEUNIT_MS);
            if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            {
                ERRCHECK(result);
            }
            Common_Draw("Temps %02d:%02d:%02d/%02d:%02d:%02d : %s", ms / 1000 / 60, ms / 1000 % 60, ms / 10 % 100, lenms / 1000 / 60, lenms / 1000 % 60, lenms / 10 % 100, paused ? "Pause  " : playing ? "Lecture" : "Arret  ");

        }

        Common_Sleep(50);

    }
    while (!Common_BtnPress(BTN_QUIT));

    /* Libération et fermeture du système audio */

    result = sound->release();
    ERRCHECK(result);
    result = system->close();
    ERRCHECK(result);
    result = system->release();
    ERRCHECK(result);

    Common_Close();

    return 0;
}



