INCLUDEPATH += $$PWD
QT += core gui opengl

SOURCES += $$PWD/openglwindow.cpp \
    gamewindow.cpp \
    camera.cpp
HEADERS += $$PWD/openglwindow.h \
    gamewindow.h \
    camera.h

SOURCES += \
    main.cpp

target.path = .
INSTALLS += target

RESOURCES += \
    gestionnaire.qrc

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp
