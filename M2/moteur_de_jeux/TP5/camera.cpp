#include "camera.h"

/*
 * Paramètres : aucun
 * Retour : aucun
 * Créé une instance la classe Camera
 */
Camera::Camera()
{
    etat = 0;
    rotX = -135.0;
    rotY = -45.0;
    zoom = 1.0f;
}

/*
 * Paramètres : aucun
 * Retour : un entier représentant le mode de dessin de la scène
 */
int Camera::getEtat()
{
    return etat;
}

/*
 * Paramètres : aucun
 * Retour : un flotant représentant la rotation du terrain selon l'axe X
 */
float Camera::getRotX()
{
    return rotX;
}

/*
 * Paramètres : aucun
 * Retour : un flotant représentant la rotation du terrain selon l'axe Y
 */
float Camera::getRotY()
{
    return rotY;
}

/*
 * Paramètres : aucun
 * Retour : un flotant représentant le niveau de zoom de la scène
 */
float Camera::getZoom()
{
    return zoom;
}

/*
 * Paramètres : aucun
 * Retour : aucun
 * Change le mode de rendu de la scène
 */
void Camera::incrEtat()
{
    etat++;
    if(etat > 5)
        etat = 0;
}

/*
 * Paramètres : incr : une valeur d'angle entre 0 et 1
 * Retour : aucun
 * Modifie la rotation autour de l'axe X de incr*360 degrés
 */
void Camera::incrRotX(float incr)
{
    rotX += incr;
}

/*
 * Paramètres : incr : une valeur d'angle entre 0 et 1
 * Retour : aucun
 * Modifie la rotation autour de l'axe Y de incr*360 degrés
 */
void Camera::incrRotY(float incr)
{
    rotY += incr;
}

/*
 * Paramètres : incr : une valeur de zoom
 * Retour : aucun
 * Modifie la valeur du zoom de incr
 */
void Camera::incrZoom(float incr)
{
    zoom += incr;
}
