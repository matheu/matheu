#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include "openglwindow.h"
#include "camera.h"

#include <QElapsedTimer>
#include <QGLBuffer>

struct point
{
    float* x;
    float* y;
    float* z;
    float* colorR;
    float* colorG;
    float* colorB;
};



class GameWindow : public OpenGLWindow
{
public:
    GameWindow(int pFPS, Camera *cam, int partAmount, short sais, short drawMet);

    void initialize();
    void render();
    bool event(QEvent *event);

    void keyPressEvent(QKeyEvent *event);

    void displayTriangles();
    void displayLines();
    void displayTrianglesC();
    void displayPoints();
    void displayTrianglesTexture();

    void affichierParticules();

    void loadMap(QString localPath);

    float randFloat();

private:

    int m_frame; // Retient le numéro de la dernière frame dessinée
    int particleAmount; // Nombre de particules à afficher sur le terrain
    int nbPoints; // Nombre de points du terrain
    QImage m_image; // Heightmap représentant le terrain

    point particules; // Tableau contenant les particules du terrain

    point p; // Tableau contenant les points du terrain
    QVector<QVector3D> m_vertexarray; // Liste des points de tous les triangles du terrain (affichage en VertexArray)
    QVector<GLuint> m_indices; // Liste des indices des points de tous les triangles du terrain (affichage en VBO)
    QGLBuffer m_vertexbuffer; // Buffer des vertex du terrain (affichage en VBO)
    QGLBuffer m_indicebuffer; // Buffer des indices des vertex du terrain (affichage en VBO)


    short carte; // Numéro de la carte affichée
    short FPS; // Représente le nombre de fois par seconde où la fonction de rendu doit être appelée
    short saison; // Numéro représentant la saison de la scène (0 printemps, 1 été, 2 automone et 3 hiver)
    short drawMethod; // Méthode de dessin : direct=0, VertexArray=1, VBO=2
    float altitudeMax; // Altitude du point le plus haut du terrain
    bool autoRotate; // Permet de savoir si le terrain doit tourner tout seul ou non
    bool particulesEnabled; // Active ou désactive l'affichage des particules

    Camera* camera; // Pointeur vers la caméra pertagée
    QTimer* timer; // Timer permettant d'attendre entre chaque rendu
    QElapsedTimer* timerFramerate; // Timer permettant de calculer le nombre d'images par seconde
};


#endif // GAMEWINDOW_H
