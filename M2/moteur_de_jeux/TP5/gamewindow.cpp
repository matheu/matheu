#include "gamewindow.h"

#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>

#include <QtCore/qmath.h>
#include <QMouseEvent>
#include <QKeyEvent>
#include <time.h>
#include <sys/time.h>
#include <iostream>

#include <QtCore>
#include <QtGui>
using namespace std;


/*
 * Paramètres : pFPS : nombre de fois par seconde où la fonction de rendu doit être appelée,
 *              cam : un pointeur vers une Camera partagée
 *              partAmount : nombre de particules à afficher sur le terrain
 * Retour : aucun
 * Construit une instance de la classe GameWindow
 */
GameWindow::GameWindow(int pFPS, Camera* cam, int partAmount, short sais, short drawMet) :
    m_vertexbuffer(QGLBuffer::VertexBuffer),
    m_indicebuffer(QGLBuffer::IndexBuffer)
{
    carte=1;
    FPS=pFPS;
    autoRotate=false;
    particulesEnabled=false;
    saison = sais;
    camera = cam;
    particleAmount = partAmount;
    altitudeMax=0;
    drawMethod=drawMet;
}

/*
 * Paramètres : aucun
 * Retour : aucun
 * Créé la fenêtre de rendu, le point de vue, créé le timer de rendu et appèle le chargement de l'image du terrain
 */
void GameWindow::initialize()
{
    const qreal retinaScale = devicePixelRatio();


    glViewport(0, 0, width() * retinaScale, height() * retinaScale);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1.0, 1.0, -1.0, 1.0, -100.0, 100.0);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    // Creation du timer de rendu
    timer = new QTimer();
    timer->setInterval(1000.0f/FPS);
    timer->connect(timer, SIGNAL(timeout()),this, SLOT(renderNow()));
    timer->start();

    // Creation du timer de comptage du framerate
    timerFramerate = new QElapsedTimer();
    timerFramerate->start();

    loadMap(":/heightmap-1.png");
}

/*
 * Paramètres : localPath : lien de l'image du terrain
 * Retour : aucun
 * Charge l'image du terrain et rempli de tableau de points du terrain
 */
void GameWindow::loadMap(QString localPath)
{

    if (QFile::exists(localPath)) {
        m_image = QImage(localPath);
    }


    // Création et remplissage aléatoire du tableau des points du terrain
    uint id = 0;
    p.x = new float[m_image.width() * m_image.height()];
    p.y = new float[m_image.width() * m_image.height()];
    p.z = new float[m_image.width() * m_image.height()];
    p.colorR = new float[m_image.width() * m_image.height()];
    p.colorG = new float[m_image.width() * m_image.height()];
    p.colorB = new float[m_image.width() * m_image.height()];
    QRgb pixel;
    float alt;
    nbPoints = m_image.width()*m_image.height();

    m_vertexarray = QVector<QVector3D>();

    for(int i = 0; i < m_image.width(); i++)
    {
        for(int j = 0; j < m_image.height(); j++)
        {
            pixel = m_image.pixel(i,j);

            id = i*m_image.width() +j;

            p.x[id] = ((float)i - (float)m_image.width()/2.0)/m_image.width();
            p.y[id] = ((float)j - (float)m_image.height()/2.0)/m_image.height();
            p.z[id] = 0.001f * (float)(qRed(pixel));
            if(p.z[id]>altitudeMax)
                altitudeMax=p.z[id];

            alt=p.z[id];
            if(saison == 0)
            {
                if (alt > 0.18)
                {
                    p.colorR[id]=0.95f;
                    p.colorG[id]=1.0f;
                    p.colorB[id]=1.0f;
                }
                else if (alt > 0.12)
                {
                    p.colorR[id]=0.65f + alt;
                    p.colorG[id]=0.55f + alt;
                    p.colorB[id]=0.25f + alt;
                }
                else if (alt > 0.03f)
                {
                    p.colorR[id]=0.39f + alt*2.0f;
                    p.colorG[id]=0.55f + alt*2.0f;
                    p.colorB[id]=0.1f + alt*2.0f;
                }
                else
                {
                    p.colorR[id]=0.0f;
                    p.colorG[id]=0.5f + alt*10.0f;
                    p.colorB[id]=0.75f + alt*10.0f;
                }
            }
            else if(saison == 1)
            {
                if (alt > 0.12)
                {
                    p.colorR[id]=0.48f + alt;
                    p.colorG[id]=0.46f + alt;
                    p.colorB[id]=0.32f + alt;
                }
                else if (alt > 0.03f)
                {
                    p.colorR[id]=0.31f + alt*2.0f;
                    p.colorG[id]=0.46f + alt*2.0f;
                    p.colorB[id]=0.15f + alt*2.0f;
                }
                else
                {
                    p.colorR[id]=0.0f;
                    p.colorG[id]=0.5f + alt*10.0f;
                    p.colorB[id]=0.75f + alt*10.0f;
                }
            }
            else if(saison == 2)
            {
                if (alt > 0.12)
                {
                    p.colorR[id]=0.48f + alt;
                    p.colorG[id]=0.46f + alt;
                    p.colorB[id]=0.32f + alt;
                }
                else if (alt > 0.03f)
                {
                    p.colorR[id]=0.8f + alt*2.0f + randFloat();
                    p.colorG[id]=0.55f + alt*2.0f - randFloat()*0.1f;
                    p.colorB[id]=0.05f + alt*2.0f;
                }
                else
                {
                    p.colorR[id]=0.7f;
                    p.colorG[id]=0.6f + alt*4.0f;
                    p.colorB[id]=0.7f + alt*4.0f;
                }
            }
            else
            {
                if (alt > 0.12)
                {
                    p.colorR[id]=1.0f;
                    p.colorG[id]=1.0f;
                    p.colorB[id]=1.0f;
                }
                else if (alt > 0.03f)
                {
                    p.colorR[id]=0.24f + alt*2.0f;
                    p.colorG[id]=0.24f + alt*2.0f;
                    p.colorB[id]=0.16f + alt*2.0f;
                }
                else
                {
                    p.colorR[id]=0.43f;
                    p.colorG[id]=0.5f + alt*10.0f;
                    p.colorB[id]=0.68f + alt*10.0f;
                }
            }
        }
    }

    int cpt=0;
    for(int i = 0; i < m_image.width()-1; i++)
    {
        for(int j = 0; j < m_image.height()-1; j++)
        {
            id = i*m_image.width() +j;

            // On rempli la liste des points pour l'affichage en mode VertexArray
            m_vertexarray.push_back(QVector3D(p.x[id], p.y[id], p.z[id]));
            m_vertexarray.push_back(QVector3D(p.x[id+1], p.y[id+1], p.z[id+1]));
            m_vertexarray.push_back(QVector3D(p.x[id+m_image.width()], p.y[id+m_image.width()], p.z[id+m_image.width()]));

            m_vertexarray.push_back(QVector3D(p.x[id+1], p.y[id+1], p.z[id+1]));
            m_vertexarray.push_back(QVector3D(p.x[id+m_image.width()+1], p.y[id+m_image.width()+1], p.z[id+m_image.width()+1]));
            m_vertexarray.push_back(QVector3D(p.x[id+m_image.width()], p.y[id+m_image.width()], p.z[id+m_image.width()]));

            // On rempli la liste des indices des points pour l'affichage en mode VBO
            m_indices.push_back(cpt+0);
            m_indices.push_back(cpt+1);
            m_indices.push_back(cpt+2);

            m_indices.push_back(cpt+3);
            m_indices.push_back(cpt+4);
            m_indices.push_back(cpt+5);
            cpt+=6;
        }
    }

    // Création et remplissage aléatoire du tableau des particules aléatoires
    // Bornes x et y = [-0.5, 0.5] et borne z = [altitudeMax, 1]
    particules.x = new float[particleAmount];
    particules.y = new float[particleAmount];
    particules.z = new float[particleAmount];
    for(int i = 0; i < particleAmount; i++)
    {
        particules.x[i] = randFloat()-0.5f;
        particules.y[i] = randFloat()-0.5f;
        particules.z[i] = randFloat()/(1.0f-altitudeMax)+altitudeMax;
    }

    // Creation du buffer de vertex pour VBO
    m_vertexbuffer.create();
    m_vertexbuffer.bind();
    m_vertexbuffer.allocate(m_vertexarray.constData(), m_vertexarray.size() * sizeof(QVector3D));
    m_vertexbuffer.release();

    // Creation du buffer des indices des vertex pour VBO
    m_indicebuffer.create();
    m_indicebuffer.bind();
    m_indicebuffer.allocate(m_indices.constData(), m_indices.size() * sizeof(GLuint));
    m_indicebuffer.release();
}

/*
 * Paramètres : aucun
 * Retour : aucun
 * Appelée à chaque nouveau rendu, cette méthode dessine les objets de la scène
 */
void GameWindow::render()
{
    if(m_frame%100 == 0)
    {
        std::cout << 100000/timerFramerate->elapsed() << std::endl; // 1000(ms) * 100(nbFrame) = 100000
        timerFramerate->restart();
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    glScalef(camera->getZoom(),camera->getZoom(),camera->getZoom());
    glRotatef(camera->getRotX(),1.0f,0.0f,0.0f);
    glRotatef(camera->getRotY(),0.0f,0.0f,1.0f);
    if(autoRotate)
        camera->incrRotY(1.0f);

    switch(camera->getEtat())
    {
    case 0:
        displayPoints();
        break;
    case 1:
        displayLines();
        break;
    case 2:
        displayTriangles();
        break;
    case 3:
        displayTrianglesC();
        break;
    case 4:
        displayTrianglesTexture();
        break;
    case 5:

        displayTrianglesTexture();
        displayLines();
        break;
    default:
        displayPoints();
        break;
    }
    if(particulesEnabled && (saison==2 || saison==3)) // En automne ou en hiver
        affichierParticules();


    ++m_frame;
}

/*
 * Paramètres : event : évènement utilisateur
 * Retour : vrai si un évènement utilisateur nécessite de redessiner la scène, faux sinon
 * Lance un nouveau rendu si un évènement utilisateur en créé le besoin
 */
bool GameWindow::event(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::UpdateRequest:

        //renderNow();
        return true;
    default:
        return QWindow::event(event);
    }
}

/*
 * Paramètres : event : évènement utilisateur
 * Retour : aucun
 * Ecoute l'appuie des touches du clavier
 */
void GameWindow::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case 'Z':
        camera->incrZoom(0.10f);
        break;
    case 'S':
        camera->incrZoom(-0.10f);
        break;
    case 'A':
        camera->incrRotX(1.0f);
        break;
    case 'E':
        camera->incrRotX(-1.0f);
        break;
    case 'Q':
        camera->incrRotY(1.0f);
        break;
    case 'D':
        camera->incrRotY(-1.0f);
        break;
    case 'W':
        camera->incrEtat();
        break;
    case 'C':
        autoRotate=!autoRotate;
        break;
    case 'T':
        particulesEnabled=!particulesEnabled;
        break;
    case 'G':
        drawMethod++;
        if(drawMethod > 2)
            drawMethod = 0;
        break;
    case 'P':
        FPS*=2;
        timer->setInterval(1000.0f/FPS);
        break;
    case 'M':
        FPS/=2;
        timer->setInterval(1000.0f/FPS);
        break;
    case 'X':
        carte ++;
        if(carte > 3)
            carte = 1;
        QString depth (":/heightmap-");
        depth += QString::number(carte) ;
        depth += ".png" ;

        loadMap(depth);
        break;
    }
    //renderNow();
}


/*
 * Paramètres : aucun
 * Retour : aucun
 * Dessine le terrain sous forme de points
 */
void GameWindow::displayPoints()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    uint id = 0;
    switch(drawMethod)
    {
    case 0: // Direct
        glBegin(GL_POINTS);
        for(int i = 0; i < m_image.width(); i++)
        {
            for(int j = 0; j < m_image.height(); j++)
            {
                id = i*m_image.width() +j;
                glVertex3f(
                            p.x[id],
                            p.y[id],
                            p.z[id]);

            }
        }
        glEnd();
        break;
    case 1: // Vertex Array
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, m_vertexarray.constData());
        glDrawArrays(GL_POINTS, 0, m_vertexarray.size());
        glDisableClientState(GL_VERTEX_ARRAY);
        break;
    case 2: // VBO

        break;
    }
}


/*
 * Paramètres : aucun
 * Retour : aucun
 * Dessine le terrain sous forme de triangles blancs
 */
void GameWindow::displayTriangles()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    uint id = 0;

    switch(drawMethod)
    {
    case 0: // Direct
        glBegin(GL_TRIANGLES);
        for(int i = 0; i < m_image.width()-1; i++)
        {
            for(int j = 0; j < m_image.height()-1; j++)
            {

                id = i*m_image.width() +j;
                glVertex3f(
                            p.x[id],
                            p.y[id],
                            p.z[id]);
                id = i*m_image.width() +(j+1);
                glVertex3f(
                            p.x[id],
                            p.y[id],
                            p.z[id]);
                id = (i+1)*m_image.width() +j;
                glVertex3f(
                            p.x[id],
                            p.y[id],
                            p.z[id]);


                id = i*m_image.width() +(j+1);
                glVertex3f(
                            p.x[id],
                            p.y[id],
                            p.z[id]);
                id = (i+1)*m_image.width() +j+1;
                glVertex3f(
                            p.x[id],
                            p.y[id],
                            p.z[id]);
                id = (i+1)*m_image.width() +j;
                glVertex3f(
                            p.x[id],
                            p.y[id],
                            p.z[id]);
            }
        }

        glEnd();
        break;
    case 1: // Vertex Array
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, m_vertexarray.constData());
        glDrawArrays(GL_TRIANGLES, 0, m_vertexarray.size());
        glDisableClientState(GL_VERTEX_ARRAY);
        break;
    case 2: // VBO
        glEnableClientState(GL_VERTEX_ARRAY);
        m_vertexbuffer.bind();
        glVertexPointer(3, GL_FLOAT, 0, NULL);
        m_vertexbuffer.release();

        m_indicebuffer.bind();
        glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, NULL);
        m_indicebuffer.release();
        glDisableClientState(GL_VERTEX_ARRAY);
        break;
    }

}

/*
 * Paramètres : aucun
 * Retour : aucun
 * Dessine le terrain sous forme de triangles blancs et verts
 */
void GameWindow::displayTrianglesC()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_TRIANGLES);
    uint id = 0;

    for(int i = 0; i < m_image.width()-1; i++)
    {
        for(int j = 0; j < m_image.height()-1; j++)
        {
            glColor3f(0.0f, 1.0f, 0.0f);
            id = i*m_image.width() +j;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = i*m_image.width() +(j+1);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = (i+1)*m_image.width() +j;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);


            glColor3f(1.0f, 1.0f, 1.0f);
            id = i*m_image.width() +(j+1);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = (i+1)*m_image.width() +j+1;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = (i+1)*m_image.width() +j;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
        }
    }
    glEnd();
}


/*
 * Paramètres : aucun
 * Retour : aucun
 * Dessine le terrain sous forme de triangles en fil de fer
 */
void GameWindow::displayLines()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_LINES);
    uint id = 0;

    for(int i = 0; i < m_image.width()-1; i++)
    {
        for(int j = 0; j < m_image.height()-1; j++)
        {

            id = i*m_image.width() +j;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = i*m_image.width() +(j+1);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);

            id = (i+1)*m_image.width() +j;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = i*m_image.width() +j;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);

            id = (i+1)*m_image.width() +j;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = i*m_image.width() +(j+1);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);

            id = i*m_image.width() +(j+1);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = (i+1)*m_image.width() +j+1;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);

            id = (i+1)*m_image.width() +j+1;
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);

            id = (i+1)*m_image.width() +(j);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
        }
    }

    glEnd();
}

/*
 * Paramètres : aucun
 * Retour : aucun
 * Dessine le terrain sous forme de triangles dont la couleur représente l'altitude du terrain
 */
void GameWindow::displayTrianglesTexture()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_TRIANGLES);
    uint id = 0;

    for(int i = 0; i < m_image.width()-1; i++)
    {
        for(int j = 0; j < m_image.height()-1; j++)
        {

            id = i*m_image.width() +j;
            glColor3f(p.colorR[id], p.colorG[id], p.colorB[id]);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = i*m_image.width() +(j+1);
            glColor3f(p.colorR[id], p.colorG[id], p.colorB[id]);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = (i+1)*m_image.width() +j;
            glColor3f(p.colorR[id], p.colorG[id], p.colorB[id]);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);



            id = i*m_image.width() +(j+1);
            glColor3f(p.colorR[id], p.colorG[id], p.colorB[id]);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = (i+1)*m_image.width() +j+1;
            glColor3f(p.colorR[id], p.colorG[id], p.colorB[id]);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
            id = (i+1)*m_image.width() +j;
            glColor3f(p.colorR[id], p.colorG[id], p.colorB[id]);
            glVertex3f(
                        p.x[id],
                        p.y[id],
                        p.z[id]);
        }
    }
    glEnd();
}


/*
 * Paramètres : aucun
 * Retour : aucun
 * Dessine des particules sur le terrain
 */
void GameWindow::affichierParticules()
{
    if(saison==2)
        glColor3f(0.25f, 0.6f, 1.0f); // automne
    else
        glColor3f(0.9f, 0.95f, 1.0f); // hiver


    int i, iReverse, jReverse, idReverse;
    float xPoint, yPoint;
    #pragma omp parallel shared(i) private(iReverse, jReverse, idReverse, xPoint, yPoint)
    {
        #pragma omp for
        for(i = 0; i < particleAmount; i++)
        {
            // On fait en sorte que la particule ne puisse pas traverser le sol
            xPoint = (float)floor((int)(particules.x[i]*(float)m_image.width()))/(float)m_image.width();
            yPoint = (float)floor((int)(particules.y[i]*(float)m_image.height()))/(float)m_image.height();

            iReverse = (int)(xPoint*(float)m_image.width() + (float)m_image.width()/2.0f);
            jReverse = (int)(yPoint*(float)m_image.height() + (float)m_image.height()/2.0f);

            idReverse = iReverse*m_image.width() + jReverse;
            idReverse = idReverse<0?0:idReverse>=nbPoints?nbPoints-1:idReverse;

            // On fait descendre les particules
            if(saison==2)
                 particules.z[i]-=0.02f; // pluie
            else
            {
                particules.x[i]+=(randFloat()-0.5f)/200; // neige
                particules.y[i]+=(randFloat()-0.5f)/200;
                particules.z[i]-=0.004f;

                particules.x[i] = particules.x[i]<-0.5f?-0.5f:particules.x[i]>0.5f?0.5f:particules.x[i];
                particules.y[i] = particules.y[i]<-0.5f?-0.5f:particules.y[i]>0.5f?0.5f:particules.y[i];
            }

            if(particules.z[i]<=p.z[idReverse])
                particules.z[i]=1.0f;
        }
    }

    glBegin(GL_POINTS);
    for(int i = 0; i < particleAmount; i++)
    {
        glVertex3f(
                    particules.x[i],
                    particules.y[i],
                    particules.z[i]);
    }
    glEnd();
}

/*
 * Paramètres :auncun
 * Retour : Un flotant aléatoire entre et 0 (inclu) et 1 (inclu)
 */
float GameWindow::randFloat()
{
    return static_cast <float> (qrand()) / static_cast <float> (RAND_MAX);
}
