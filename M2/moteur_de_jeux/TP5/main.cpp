#include "gamewindow.h"

#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>

#include <QtCore/qmath.h>
#include <QMouseEvent>
#include <QKeyEvent>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <omp.h>

#include <QtCore>
#include <QtGui>
#include "camera.h"

using namespace std;



int main(int argc, char **argv)
{
    srand(time(NULL));
    QGuiApplication app(argc, argv);

    QSurfaceFormat format;
    format.setSamples(16);

    // On instancie une caméra unique qui sera partagée par les fenêtres
    Camera* camera = new Camera();

    // On créé les quatre fenêtres de rendu
    /*GameWindow* window = new GameWindow(45, camera, 0, 0);
    window->setFormat(format);
    window->resize(640, 480);
    window->show();
    window->setAnimating(true);


    GameWindow* window2 = new GameWindow(45, camera, 0, 1);
    window2->setFormat(format);
    window2->resize(640, 480);
    window2->show();
    window2->setAnimating(true);


    GameWindow* window3 = new GameWindow(45, camera, 50000, 2);
    window3->setFormat(format);
    window3->resize(640, 480);
    window3->show();
    window3->setAnimating(true);
*/

    GameWindow* window4 = new GameWindow(9999, camera, 10000, 3, 0);
    window4->setFormat(format);
    window4->resize(640, 480);
    window4->show();
    window4->setAnimating(true);


    return app.exec();
}



