#ifndef CAMERA_H
#define CAMERA_H

class Camera
{
public:
    Camera();
    int getEtat();
    float getRotX();
    float getRotY();
    float getZoom();

    void incrEtat();
    void incrRotX(float incr);
    void incrRotY(float incr);
    void incrZoom(float incr);

private:
    int etat; // Représente le mode de dessin de la scène
    float rotX; // Représente la rotation du terrain selon l'axe X
    float rotY; // Représente la rotation du terrain selon l'axe Y
    float zoom; // Représente le niveau de zoom sur la scène

};


#endif // CAMERA_H
