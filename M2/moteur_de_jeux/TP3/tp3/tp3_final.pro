INCLUDEPATH += .
SOURCES += ./openglwindow.cpp \
    GameWindow.cpp
HEADERS += ./openglwindow.h \
    GameWindow.h

SOURCES += \
    main.cpp

target.path = .

INSTALLS += target
QMAKE_CXXFLAGS+= -fopenmp
QMAKE_LFLAGS  += -fopenmp


RESOURCES += \
    gestionnaire.qrc
