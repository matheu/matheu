

#ifndef MATRICE_H
#define MATRICE_H
#include<QVector>

class Matrice
{
    int n;
    QVector< QVector< int > > mat ;
    int det;
    int determinantetape();
public:

    Matrice(int n);
    int xy(int x,int y);
    void modif(int x, int y, int val);
    Matrice multiplication(Matrice m);
    int size();
    int determinant();
    int deter();
    void toString();
    void multiplicoef(int n);
    Matrice inverse();
    Matrice transpose();
};

#endif // MATRICE_H



