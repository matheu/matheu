#include <QCoreApplication>
#include<stdio.h>
#include<vecteur.h>
#include<iostream>
#include<matrice.h>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Vecteur v1 = Vecteur(2);
    Vecteur v2 = Vecteur(2);
    v1.modif(0,1);
    v1.modif(1,4);
    v2.modif(0,-2);
    v2.modif(1,7);
    v1.toString();
 //   v2.toString();
 /*   Vecteur v3 = v1.addition(v2);
    v3.toString();
    v3 = v1.soustraction(v2);
    v3.toString();
*/

/*    Matrice m1 = Matrice(2);
    Matrice m2 = Matrice(2);
    m1.modif(0,0,1);
    m1.modif(0,1,2);
    m1.modif(1,0,3);
    m1.modif(1,1,4);
    m2.modif(0,0,1);
    m2.modif(0,1,-2);
    m2.modif(1,0,0);
    m2.modif(1,1,1);
    m1.toString();*/
//    m2.toString();
 /*   Matrice m3 = m1.multiplication(m2);
    cout<<endl;*/
//    m3.toString();


/*    Vecteur v3 = v1.multiplicationMatriciel(m1);
    cout<<endl;
    v3.toString();
*/

 /*   Matrice m3 = m1.transpose();
    cout<<endl;
    m3.toString();*/
    Matrice m4 = Matrice(3);
    m4.modif(0,0,-1);m4.modif(0,1,2);m4.modif(0,2,5);
    m4.modif(1,0,1);m4.modif(1,1,2);m4.modif(1,2,3);
    m4.modif(2,0,-2);m4.modif(2,1,8);m4.modif(2,2,10);
    cout<<endl;
    m4.toString();
    cout<<m4.determinant()<<endl;
    Matrice m5 = m4.inverse();
    m5.toString();

    return a.exec();
}
