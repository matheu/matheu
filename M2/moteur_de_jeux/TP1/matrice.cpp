
#include "matrice.h"
#include<QVector>
#include<stdio.h>
#include<iostream>
#include<math.h>

using namespace std;


Matrice::Matrice(int n)
{
    this->n=n;
    mat = QVector< QVector< int > >(n);
    for(int i=0;i<n;i++)
    {
        mat[i] = QVector< int >(n);
    }
}

int Matrice::size()
{
    return this->n;
}

int Matrice::xy(int x,int y)
{
    return mat[x][y];
}

int Matrice::deter()
{
    return this->det;
}

void Matrice::modif(int x, int y, int val)
{
    if(x<this->n and x>-1 and y<this->n and y>-1)
    {
        this->mat[x][y]=val;
    }
    else
    {
        cout<<"erreur de taille";
    }
}


int Matrice::determinantetape()
{
    int rez = 0;
    for(int i=0;i<this->size();i++)
    {
        Matrice m = Matrice(size()-1);
        for(int j=1;j<size();j++)
            for(int k=0;k<size();k++)
            {
                if(k!=i)
                    m.modif(j-1,((k<i)?(k):(k-1)),this->xy(j,k));
            }

        rez += pow(-1,i)*this->xy(0,i)*m.determinant();
    }
    this->det = rez;
    return this->det;
}

int Matrice::determinant()
{
    if(this->size()!=2)
    {
        this->det=this->determinantetape();
    }
    else
    {
        this->det = (this->xy(0,0)*this->xy(1,1)-(this->xy(0,1)*this->xy(1,0)));
    }
    return this->deter();
}

Matrice Matrice::multiplication(Matrice m)
{
    if(m.size()==this->n)
    {
        Matrice mat = Matrice(this->n);
        for(int i=0;i<this->n;i++)
            for(int j=0;j<this->n;j++)
            {
                int value =0;
                for(int k=0;k<this->n;k++)
                {
                    value += this->xy(i,k)*m.xy(k,j);
                }
                mat.modif(i,j,value);
            }
        return mat;
    }
    else
    {
         cout<<"erreur de taille";
    }
}

void Matrice::multiplicoef(int coef)
{
    for(int i=0;i<this->size();i++)
        for(int j=0;j<this->size();j++)
        {
            this->modif(i,j,this->xy(i,j)*coef);
        }
}

Matrice Matrice::transpose()
{
    Matrice m= Matrice(this->n);
    for(int i=0;i<this->n;i++)
        for(int j=0;j<this->n;j++)
            m.modif(i,j,this->xy(j,i));
    return m;
}


Matrice Matrice::inverse()
{
    if(this->determinant()!=0)
    {
        Matrice m = Matrice(this->size());
        for(int i=0;i<this->size();i++)
            for(int j=0;j<this->size();j++)
            {
                Matrice mprime = Matrice(this->size()-1);
                for(int l=0;l<size();l++)
                    for(int k=0;k<size();k++)
                    {
                        if(k!=j&&l!=i)
                            m.modif(((l<i)?(l):(l-1)),((k<j)?(k):(k-1)),this->xy(j,k));
                    }
                m.modif(i,j,mprime.determinant());
            }
        m.transpose();
        m.multiplicoef(this->deter());
        return m;
    }
    else
    {
        cout<<"Matrice non inversible"<<endl;
        return 0;
    }
}



void Matrice::toString()
{
    for(int i=0;i<this->n;i++)
    {
        for(int j=0;j<this->n;j++)
            cout<<this->xy(i,j)<<" ";
        cout<<endl;
    }
}


