#include "vecteur.h"
#include<QVector>
#include<stdio.h>
#include<iostream>

using namespace std;


Vecteur::Vecteur(int n)
{
    this->n=n;
    vect = QVector< int > (n);
}

int Vecteur::size()
{
    return this->n;
}

int Vecteur::val(int x)
{
    return vect[x];
}

void Vecteur::modif(int x, int val)
{
    if(x<this->n and x>-1)
    {
        this->vect[x]=val;
    }
    else
    {
        cout<<"erreur de taille";
    }
}

Vecteur Vecteur::addition(Vecteur v)
{
    if(this->n==v.size())
    {

        Vecteur vec = Vecteur(this->n);
        for(int i=0;i<this->n;i++)
        {
            vec.modif(i,(v.val(i)+this->val(i)));
        }
        return vec;
    }
    else
        cout<<"vecteur non additionable";
}

Vecteur Vecteur::soustraction(Vecteur v)
{
    if(this->n==v.size())
    {

        Vecteur vec = Vecteur(this->n);
        for(int i=0;i<this->n;i++)
        {
            vec.modif(i,(this->val(i)-v.val(i)));
        }
        return vec;
    }
    else
        cout<<"vecteur non additionable";
}

Vecteur Vecteur::multiplicationMatriciel(Matrice m)
{
    if(this->n==m.size())
    {
        Vecteur vec = Vecteur(this->n);
        for(int i = 0;i<this->n;i++)
        {
            int value =0;
            for(int j=0;j<this->n;j++)
                value+=this->val(j)*m.xy(i,j);
            vec.modif(i,value);
        }
        return vec;
    }
    else
        cout<<"erreur de taile";
}

void Vecteur::toString()
{
    cout<<"taille: "<<this->n<<endl;
    for(int i=0;i<this->n;i++)
    {
        cout<<this->val(i)<<endl;
    }
}
