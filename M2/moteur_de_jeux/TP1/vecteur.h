#ifndef VECTEUR_H
#define VECTEUR_H
#include<QVector>
#include<matrice.h>

class Vecteur
{
    int n;
    QVector< int > vect;
public:


    Vecteur(int n);
    void modif(int x, int val);
    int val(int x);
    Vecteur addition(Vecteur v);
    void toString();
    int size();
    Vecteur soustraction(Vecteur v);
    Vecteur multiplicationMatriciel(Matrice m);

};

#endif // VECTEUR_H
