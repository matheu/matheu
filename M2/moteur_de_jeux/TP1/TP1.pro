#-------------------------------------------------
#
# Project created by QtCreator 2014-09-19T15:44:36
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = TP1
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    matrice.cpp \
    vecteur.cpp

HEADERS += \
    matrice.h \
    vecteur.h
