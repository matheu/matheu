#include <cstdlib>
#include <iostream>
#include<iomanip>
#include<sstream>
#include <vector>
#include "Graphe/Graphe.h"
using namespace std;
;
void cls()
{
	    cout<<"\033[2J\033[H";
}

bool preprocessing(Graphe g,  int k)
{
    int n=g.nbSommet();

        for(int i=0;i<n;i++)            //si il exite un sommet de degrés 0 alors il n'est pas dans le vertex cover
    {
        if(g.voisins(i).size()==0)
        {
            Graphe g1(g,i);
            //g.delSommet(i);
            return preprocessing(g1,k) ;
        }
    }

    for(int i=0;i<n;i++)            //si il exite un sommet de degrés 1 alors son voisin est dans le vertex cover
    {
        if(g.voisins(i).size()==1)
        {
            int x=g.voisins(i)[0];
            g.delVoisSommet(i);
            return preprocessing(g,k-1) ;
        }
    }
        for(int i=0;i<n;i++)            //si il exite un sommet de degrés supèrieur à k+1 alors son voisin est dans le vertex cover
    {
        if(g.voisins(i).size()>=k+1)
        {
            Graphe g1(g,i);
            return preprocessing(g1,k-1) ;
        }
    }
	if(g.nbArete()>k*k)
		return false;
	return true;

}



bool VCexaustif(Graphe g,int k)
{
	int n=g.nbSommet();
	if(k>=n)
		return true;
	bool fini = false;
	bool trouver = false;
	int tab[k];
	for(int i=0;i<k;i++)
		tab[i]=i;
	while(!fini)
	{
		if(tab[k-1]==n)
		{
			int i=k-2;
			while(i!=-1 and tab[i]==((n-1)-((k-1)-i)))
				i--;
			if(i==-1)
				fini=true;
			else
			{
				tab[i]++;
				for(int j=i+1;j<k;j++)
				{
					tab[j]=tab[j-1]+1;
				}
			}
		}

		if(tab[k-1]!=n)
		{
			Graphe gcopie(g);
			for(int i=0;i<k;i++)
				gcopie.delSommet(tab[i]);
			if(gcopie.nbArete()==0)
			{
				fini=true;
				trouver=true;
			}
			if(!trouver)
				tab[k-1]++;
		}
	}
	if(trouver)
		for(int i=0;i<k;i++)
			cout<<tab[i]<<" ";
		cout<<endl;
	return (trouver);
}

bool VCbranchement(Graphe g,int k)
{
        if(g.nbArete()==0)
        return true;
        if(g.nbArete()>=k*g.nbSommet())
        return false;

    /*	if(k>=0 and g.nbArete()==0)
    	{
        	return true;
    	}
    	if(k<0)
    	{
        	return false;
    	}*/
	vector<int> vect;
	//vect.resize(2);
	vect = g.areteFirst();
    	int x=vect[0];
    	int y=vect[1];
	Graphe g2(g);g2.delSommet(x);
	Graphe g3(g);g3.delSommet(y);



    	return (VCbranchement(g2,k-1) or VCbranchement(g3,k-1));
}


bool VCbranchementplus(Graphe g,int k)
{

	if(k>=0 and g.nbArete()==0)
	{
    	    	return true;
    	}
    	if(k<0)
    	{
        	return false;
    	}
    	int x=g.sommetMaxDeg();
    	Graphe g2(g);
    	g2.delSommet(x);
    	Graphe g3(g);
    	vector<int> listvois =g.voisins(x);
    	int taille=listvois.size();
    	for(int i=0;i<taille;i++)
    		g3.delSommet(listvois[i]);
    	return (VCbranchementplus(g2,k-1) or VCbranchementplus(g3,k-(taille)));
}

bool VC_N(Graphe& g,int k)
{

    if(k>=0 and g.nbArete()==0)
    {
        return true;
    }
    if(k<0 )
    {
        return false;
    }
    int n=g.nbSommet();

    for(int i=0;i<n;i++)            //si il exite un sommet de degrés 1 alors son voisin et dans le vertex cover
    {
        if(g.degSom(i)==1)
        {   
            Graphe g2(g,i);
            return VC_N(g2,k-1) ;
        }
    }

     for(int i=0;i<n;i++)       //on regarde les sommets de degrés supèrieur à 5
    {
        int t=g.degSom(i);
        if(t>4)
        {
            Graphe g1(g,i);
            Graphe g2(g); //erreur on enleve pas les voisins
            return (VC_N(g2,k-t) or VC_N(g1,k-1)) ;
        }
    }

    unsigned int d=g.voisins(0).size();
    int cpt=1;
    for(int i=1;i<n;i++)    //on regarde si le graphe est régulier
    {
        if(d==g.degSom(i))
        {
            cpt++;
        }
    }

    if(cpt==n)
    {
            int x=g.areteFirst()[0];
            int t=g.voisins(x).size();
            Graphe g1(g,x);
            g.delVoisSommet(x);//corrigé cela avec mathias
            return (VC_N(g,k-t) or VC_N(g1,k-1)) ;
    }


    for(int i=0;i<n;i++)       //on regarde les sommets de degrés égale à 2
    {
        if(g.degSom(i)==2)
        {
            int a=g.voisins(i)[0];
            int b=g.voisins(i)[1];
            if(g.voisin(a,b))
            {
                Graphe g1(g,a);
                Graphe g2(g,b);
                return (VC_N(g2,k-1) or VC_N(g1,k-1)) ;
            }
            else
            {
                int ta=g.voisins(a).size();
                bool k=false;
                int c;
                for(int j=0;j<ta;j++)
                {
                    if(g.voisin(b,g.voisins(a)[j]) and g.voisins(a)[j]!=i )
                        {
                            k=true;
                            c=g.voisins(a)[j];
                        }
                }
                if((g.degSom(a)==g.degSom(b)) and (g.degSom(a)==2) and k)
                {

                    Graphe g3(g,c);
                    g.delSommet(i);//enlever les deux sommets un branchement en non deux
                    return (VC_N(g,k-1) or VC_N(g3,k-1)) ;
                }
                else
                {

                    Graphe g2(g);
                    Graphe g3(g);
                    g3.delVoisSommet(i);
                    g2.delVoisSommet(a);
                    g2.delVoisSommet(b);
                    return (VC_N(g3,k-1) or VC_N(g2,k-2) ) ;//pas 2 mais la somme de l'union des vois de a et b
                }
            }
        }
    }

    for(int i=0;i<n;i++)       //on regarde les sommets de degrés égale à 3
    {
        if(g.degSom(i)==3)
        {
            int a=g.voisins(i)[0];
            int b=g.voisins(i)[1];
            int c=g.voisins(i)[2];
            int da=g.degSom(a);
            int db=g.degSom(b);
            int dc=g.degSom(c);
            //on cherche a savoir si i forme un triangle avec 2 de ses voisins
            if(g.voisin(a,b))
              {
                Graphe g2(g);
                g.delVoisSommet(i);
                g2.delVoisSommet(c);
                return (VC_N(g,k-g.degSom(i)) or VC_N(g2,k-dc) ) ;//enlever seulement 1 ?????
              }
               else if(g.voisin(a,c))
                       {
                           Graphe g2(g);
                            g.delVoisSommet(i);
                            g2.delVoisSommet(b);
                            return (VC_N(g,k-g.degSom(i)) or VC_N(g2,k-db) ) ;
                       }
                        else if(g.voisin(b,c))
                                    {
                                        Graphe g2(g);
                                        g.delVoisSommet(i);
                                        g2.delVoisSommet(a);
                                        return (VC_N(g,k-g.degSom(i)) or VC_N(g2,k-da) ) ;
                                    }

            //recherche d'un cycle de longueur 4 contenant i , deux de ses voisins (a,b; a,c ou b,c) et un autre sommet n'étant pas voisin avec i (on le note e)
            int e;
            for(int la=0;la<da;la++)
            {
                for(int lb=0;lb<db;lb++)
                {
                    if (g.voisin(g.voisins(a)[da],g.voisins(b)[db]))
                    {
                        e=g.voisins(a)[da];
                        if(!g.voisin(e,i))
                        {
                            Graphe g2(g);
                            g.delVoisSommet(i);
                            g2.delSommet(i);
                            g2.delSommet(e);
                            return (VC_N(g,k-g.degSom(i)) or VC_N(g2,k-2) ) ;
                        }

                    }
                }
            }
                        for(int la=0;la<da;la++)
            {
                for(int lc=0;lc<dc;lc++)
                {
                    if (g.voisin(g.voisins(a)[da],g.voisins(c)[dc]))
                    {
                        e=g.voisins(a)[da];
                        if(!g.voisin(e,i))
                        {
                            Graphe g2(g);
                            g.delVoisSommet(i);
                            g2.delSommet(i);
                            g2.delSommet(e);
                            return (VC_N(g,k-g.degSom(i)) or VC_N(g2,k-2) ) ;
                        }

                    }
                }
            }

            for(int lc=0;lc<dc;lc++)
            {
                for(int lb=0;lb<db;lb++)
                {
                    if (g.voisin(g.voisins(c)[dc],g.voisins(b)[db]))
                    {
                        e=g.voisins(c)[dc];
                        if(!g.voisin(e,i))
                        {
                            Graphe g2(g);
                            g.delVoisSommet(i);
                            g2.delSommet(i);
                            g2.delSommet(e);
                            return (VC_N(g,k-g.degSom(i)) or VC_N(g2,k-2) ) ;
                        }

                    }
                }
            }

            //si i est le seul voisin commun à a,b et c (ses 3 voisins ) et que l'un de ses sommet et de degré 4
            bool pasdecommun=true;
            bool d4=false;
            int ta,tb,tc,dta,dtbc;

            if(da==4)
            {
                d4=true;
                ta=a;
                tb=b;
                tc=c;
                dta=g.voisins(a).size();
                dtbc=g.voisins(c).size()+g.voisins(b).size();
            }
            else
            {
                if(db==4)
                {
                    d4=true;
                    ta=b;
                    tb=a;
                    tc=c;
                    dta=g.voisins(b).size();
                    dtbc=g.voisins(c).size()+g.voisins(a).size();
                }
                else
                {
                    if(dc==4)
                    {
                        d4=true;
                        ta=c;
                        tb=b;
                        tc=a;
                        dta=g.voisins(c).size();
                        dtbc=g.voisins(a).size()+g.voisins(b).size();
                    }
                }
            }

      for(int la=0;la<da;la++)
            {
                for(int lb=0;lb<db;lb++)
                {
                    for(int lc=0;lc<dc;lc++)
                        {
                            if((g.voisins(b)[lb]==g.voisins(a)[la] and g.voisins(b)[lb]==g.voisins(c)[lc]) and (g.voisins(c)[lc]!=i))
                            pasdecommun=false;
                        }
                }
            }

            if(pasdecommun and d4)
            {
                Graphe g2(g);
                Graphe g3(g);
                g.delVoisSommet(i);
                g2.delSommet(ta);
                g2.delVoisSommet(tb);
                g2.delVoisSommet(tc);
                g3.delVoisSommet(ta);
                return ((VC_N(g,k-g.degSom(i)) or VC_N(g2,k-1-dtbc)) or VC_N(g3,k-dta) ) ;
            }

        }
    }
    exit(-1);
}

string int2string(int i)
{
	ostringstream o;
	o<<i;
	return o.str(); 
}

void suite(Graphe g)
{
int test =1;
	bool graf=true;
	while(graf)
	{
		bool fini=false;
		int a,b,k,l;
		cout<<endl<<endl<<"Resolution du Vertex Cover"<<endl<<endl;
		cout<<"choisissez la taille du Vertex Cover"<<endl;
		cin>>k;
		l=k;
		cout<<endl<<"choisissez la methode de resolution du Vertex Cover"<<endl;
		cout<<"1_Exaustif"<<endl<<"2_Branchement"<<endl<<"3_Branchement+"<<endl<<"4_Branchement++"<<endl;
		cin>>a;
		cout<<endl<<endl<<"voulez vous ajoutez un preprocessing? 0_non , 1_oui"<<endl;
		cin>>b;
		if(b==1)
		{
			fini = !(preprocessing(g,l));
			if(fini)
				cout<<"Il n'existe pas de vertex cover possedant "<<k<<" sommets par preprocessing"<<endl;
			else if(g.nbArete()==0)
				cout<<"Il existe un vertex cover possedant "<<k<<" sommets par preprocessing"<<endl;
		}
		if(!fini)
		switch (a) 
		{
			case 1:cout<<"Resultat de Exaustif :"<<endl;
				if(VCexaustif(g,l))
	    			{
	    			    cout<<"Il existe un vertex cover possedant "<<k<<" sommets"<<endl;
	    			}
	    			else
	    			{
	        			cout<<"Il n'existe pas de vertex cover possedant "<<k<<" sommets"<<endl;
	    			}
				break;
	
			case 2:cout<<"Resultat de Branchement :"<<endl;
				if(VCbranchement(g,l))
	    			{
	    			    cout<<"Il existe un vertex cover possedant "<<k<<" sommets"<<endl;
	    			}
	    			else
	    			{
	        			cout<<"Il n'existe pas de vertex cover possedant "<<k<<" sommets"<<endl;
	    			}
				break;
	
			case 3:cout<<"Resultat de Branchement+ :"<<endl;
				if(VCbranchementplus(g,l))
	    			{
	    			    cout<<"Il existe un vertex cover possedant "<<k<<" sommets"<<endl;
	    			}
	    			else
	    			{
	        			cout<<"Il n'existe pas de vertex cover possedant "<<k<<" sommets"<<endl;
	    			}
				break;
	
			case 4:cout<<"Resultat de Branchement++ :"<<endl;
				if(VC_N(g,l))
	    			{
	    			    cout<<"Il existe un vertex cover possedant "<<k<<" sommets"<<endl;
	    			}
	    			else
	    			{
	        			cout<<"Il n'existe pas de vertex cover possedant "<<k<<" sommets"<<endl;
	    			}
				break;
	
			default:break;
		}
		cout<<endl<<endl;
		cout<<"1 : si vous voulez continuer avec le meme graphe"<<endl;
    		cout<<"0 : pour changer de graphe ou quitter"<<endl;
    		cin>>a;
    		graf = (a==1);
		cls();
	}

}
	
int main()
{
    	bool fini = false;
    	while(!fini)
    	{
    		cout<<"1 : si vous voulez construire le graphe avec des probalités pour chaque arete"<<endl;
    		cout<<"2 : si vous voulez construire le graphe tel qu'il soit regulier"<<endl;
    		int a;
    		cin>>a;
    		cls();
    		int n,k;
    		cout<<"Entrer le nombre du sommet du graphe:";
    		cin>>n;
    		double p;
    		if(a==1)
    		{
			double p;
    			cout<<"Entrer la probabilite qu'il y ai une arete entre chaque pair de sommet du graphe:";
    			cin>>p;
    			Graphe g(n,p);
			cout<<"1 : si vous voulez afficher le graphe"<<endl;
			cout<<"0 : si vous ne voulez pas afficher le graphe"<<endl;
    			cin>>p;
			cls();
			if(p==1)
			{
				g.toString();
				g.affichageGraphique("graphe_affichage");
			}
			suite(g);
    		}
    		if(a==2)
    		{
			int p;
    			cout<<"Entrer le degre voulu:";
    			cin>>p;
			Graphe g(n,p);
			cout<<"1 : si vous voulez afficher le graphe"<<endl;
			cout<<"0 : si vous ne voulez pas afficher le graphe"<<endl;
    			cin>>p;
			cls();
			if(p==1)
			{
				g.toString();
				g.affichageGraphique("graphe_affichage");
			}
			suite(g);
    		}
		cout<<endl<<endl;
    		cout<<"1 : si vous voulez creer un autre graphe"<<endl;
    		cout<<"0 : pour quitter"<<endl;
    		cin>>a;
		fini = (a==0);
		cls();
    	}
    	return 0;
}
