#include <cstdlib>
#include <iostream>
#include <vector>
#include "Graphe.cpp"
using namespace std;

bool VCbranchement(Graphe g,int k)
{
        if(g.nbArete()==0)
        return true;
        if(g.nbArete()>=k*g.nbSommet())
        return false;

    /*	if(k>=0 and g.nbArete()==0)
    	{
        	return true;
    	}
    	if(k<0)
    	{
        	return false;
    	}*/
	vector<int> vect;
	//vect.resize(2);
	vect = g.areteFirst();
    	int x=vect[0];
    	int y=vect[1];
	Graphe g2(g);g2.delSommet(x);
	Graphe g3(g);g3.delSommet(y);

	cout<<g2.nbSommet()<<endl;

    	return (VCbranchement(g2,k-1) or VCbranchement(g3,k-1));
}


int main()
{
    int n,k;
    int p;
    cout<<"Entrer le nombre du sommet du graphe:"<<endl;
    cin>>n;
    cout<<"Entrer la probabilite qu'il y ai une arete entre chaque pair de sommet du graphe:"<<endl;
    cin>>p;
    Graphe g(n,p);
    g.toString();
    g.affichageGraphique("test");
     cout<<endl<<endl;
    g.toString();
    /*cout<<"Entrer le nombre du sommet du vertex cover:"<<endl;
    cin>>k;

    if(VCbranchement(g,k))
    {
        cout<<"Il existe un vertex cover possedant "<<k<<" sommets"<<endl;
    }
    else
    {
        cout<<"Il n'existe pas de vertex cover possedant "<<k<<" sommets"<<endl;
    }*/
    return 0;
}
