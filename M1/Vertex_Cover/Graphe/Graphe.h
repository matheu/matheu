#include <vector>
#include <string>

class Graphe
{	
	public:
	
	Graphe(int n,double p);//constructeur avec probalité p qu'il y ai une arete entre chaque pair de sommet
	Graphe(int n,int deg);//constructeur avec deg le degre de chaque arete
	//ATTENTION : n*deg doit etre pair et deg<n!!! (pas fini risque de boucle infinie)
	Graphe(Graphe &g,int x);//construteur copie de G sans le sommet x
	Graphe(Graphe &g);//copie de G
	
	std::vector<int> voisins(int x);//retourne les voisins de x
	bool voisin(int x,int y);//retourne vrai si x et y sont voisins
	void delSommet(int x);//retire toutes les aretes du sommet x dans graphe
	void delSommet2(int x);//retire le sommet x du graphe
	void delVoisSommet(int x);//retire les voisins du sommet x du graphe
	void addArete(int x,int y);//creer l'arete xy
	void delArete(int x,int y);//supprime l'arete xy
	int nbSommet();//retourne n
	int nbArete();//retourne m
	void toString();//affichage console du graphe en forme matriciel
	int sommetMaxDeg();//retourne le sommet de plus haut degre
	int degSom(int x);//retourne le degre du voisin
	std::vector<int> areteFirst();//retourne le premier couple voisin dans le graphe
	void affichageGraphique(std::string fichier);//creer le 'fichier'.ps qui est un affichage du graphe
	private:
	
	int m_n,m_m;
	std::vector <std::vector <bool> > matvois; //matrice de voisins
	std::vector <std::vector <int> > listvois;//tableau de la liste de ses voisins
//	std::vector<int[2]> listarete;//liste de toutes les aretes dans un vector inutile pour le moment
};
