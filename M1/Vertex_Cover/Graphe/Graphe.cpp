#include "Graphe.h"
#include <vector>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <cassert>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include<iomanip>
#include <math.h>

#define PI 3.14159265

using namespace std;

Graphe::Graphe(int n,double p)
{
	m_m=0;
	double alea;
	srand((unsigned)time(NULL));
	m_n=n;
	matvois.resize(m_n);
	for(int i=0;i<m_n;i++)
	{
		vector<bool> add;
		matvois[i]=add;
		matvois[i].resize(m_n);
	}
	listvois.resize(m_n);
	for(int i=0;i<m_n;i++)
		for(int j=i;j<m_n;j++)
		{
			if(j!=i)
			{
				alea = ((double)rand()/(double)RAND_MAX);
				if (alea<p)
				{
					m_m++;
					matvois[i][j]=true;
					matvois[j][i]=true;
					listvois[i].push_back(j);
					listvois[j].push_back(i);
				}
				else
				{
					matvois[i][j]=false;
					matvois[j][i]=false;
				}
			}
			else
				matvois[i][j]=false;
		}
}

Graphe::Graphe(int n,int deg)
{
	if((n*deg)%2==1||deg>=n)
		cout<<"graphe impossible";
	else
	{
		m_m=(n*deg)/2;
		m_n=n;
		matvois.resize(m_n);
		for(int i=0;i<m_n;i++)
		{
			vector<bool> add;
			matvois[i]=add;
			matvois[i].resize(m_n);
		}
		srand((unsigned)time(NULL));
		listvois.resize(m_n);
		int tabdeg[m_n];
		int alea;
		for(int i=0;i<m_n;i++)
			tabdeg[i]=0;
		for(int i=0;i<m_n;i++)
		{
			while(tabdeg[i]<deg)
			{
				alea = (int)(((double)rand()/(double)RAND_MAX)*(double)n);
				int aleabis = alea;
				bool boucle = false;
				while(!boucle && (alea==i||tabdeg[alea]==deg||matvois[i][alea]))
				{
					alea++;
					if(alea==m_n)
						alea=0;
					if(alea==aleabis)
						boucle = true;
				}
				if(!boucle)
				{
					matvois[i][alea]=true;
					matvois[alea][i]=true;
					tabdeg[alea]++;
					tabdeg[i]++;
				}
				else
				{
					int k=0;
					int j=0;
					while(k<m_n && (matvois[i][k] || matvois[i][j] || !matvois[k][j]))
					{
						while(j<m_n && (matvois[i][k] || matvois[i][j] || !matvois[k][j]))
						{
							j++;
						}
						if(j==m_n)
						{
							j=0;
							k++;
						}
					}
					matvois[i][k]=true;
					matvois[k][i]=true;
					tabdeg[j]--;
					matvois[k][j]=false;
					matvois[j][k]=false;
					tabdeg[i]++;
				}
			}
		}
  for(int i=0;i<m_n;i++)
    for(int j=i+1;j<m_n;j++)
      if(matvois[i][j])
      {
          listvois[i].push_back(j);
          listvois[j].push_back(i);
      }
	}
}

Graphe::Graphe(Graphe &g,int x)
{
	bool apres=false;
	m_n=g.nbSommet()-1;
	int nbarete=0;
	matvois.resize(m_n);
	for(int i=0;i<m_n;i++)
	{
		vector<bool> add;
		matvois[i]=add;
		matvois[i].resize(m_n);
	}
	listvois.resize(m_n);
	for(int i=0;i<m_n;i++)
		for(int j=0;j<m_n;j++)
			matvois[i][j]=false;
	for(int i=0;i<x;i++)
	{
		for(int j=0;j<x;j++)
		{
			if(g.voisin(i,j))
			{
				nbarete++;
				matvois[i][j]=true;
				listvois[i].push_back(j);
			}
		}
	}
	for(int i=x+1;i<m_n+1;i++)
	{
		for(int j=0;j<x;j++)
		{
			if(g.voisin(i,j))
			{
				nbarete++;
				matvois[i-1][j]=true;
				listvois[i-1].push_back(j);
			}
		}
	}
	for(int i=0;i<x;i++)
	{
		for(int j=x+1;j<m_n+1;j++)
		{
			if(g.voisin(i,j))
			{
				nbarete++;
				matvois[i][j-1]=true;
				listvois[i].push_back(j-1);
			}
		}
	}
	for(int i=x+1;i<m_n+1;i++)
	{
		for(int j=x+1;j<m_n+1;j++)
		{
			if(g.voisin(i,j))
			{
				nbarete++;
				matvois[i-1][j-1]=true;
				listvois[i-1].push_back(j-1);
			}
		}
	}
	m_m=(int)nbarete/2;
}

Graphe::Graphe(Graphe &g)
{
	m_n=g.nbSommet();
	m_m=g.nbArete();
	matvois.resize(m_n);
	for(int i=0;i<m_n;i++)
	{
		vector<bool> add;
		matvois[i]=add;
		matvois[i].resize(m_n);
	}
	listvois.resize(m_n);
	for(int i=0;i<m_n;i++)
		for(int j=0;j<m_n;j++)
			matvois[i][j]=false;
	for(int i=0;i<m_n;i++)
	{
		listvois[i]=g.voisins(i);
		for(int j=0;j<listvois[i].size();j++)
		{
			matvois[i][listvois[i][j]]=true;
		}
	}
}

vector<int> Graphe::voisins(int x)
{
	return listvois[x];
}

bool Graphe::voisin(int x,int y)
{
	return matvois[x][y];
}

void Graphe::delSommet(int x)
{
	for(int i=0;i<m_n;i++)
	{
		if(matvois[x][i]==true)
			m_m--;
		matvois[x][i]=false;
		matvois[i][x]=false;
	}
	while(!listvois[x].empty())
	{
		int som = listvois[x].back();
		listvois[x].pop_back();
		int l = listvois[som].size();
		int i=0;
		while(i<l && listvois[som][i]!=x)
			i++;
		if(i<l)
			listvois[som].erase (listvois[som].begin()+i);
	}
}

void Graphe::delSommet2(int x)
{
	int newmat[m_n-1][m_n-1];
	int i1=0;
	int j1=0;
	for(int i=0;i<m_n;i++)
	{
		if(i!=x)
		{

			for(int j=0;j<m_n;j++)
				if(x!=j)
				{
					newmat[i1][j1]=matvois[i][j];
					j1++;
				}
			i1++;
			j1=0;
		}
	}
	matvois.resize(m_n-1);
	for(int i=0;i<m_n;i++)
	{
		vector<bool> add;
		matvois[i]=add;
		matvois[i].resize(m_n-1);
	}
  	for(int i=0;i<m_n-1;i++)
    		for(int j=0;j<m_n-1;j++)
      			matvois[i][j]=newmat[i][j];
	listvois.resize(m_n-1);
	for(int i=0;i<m_n-1;i++)
		listvois[i].clear();
	m_m=0;
	for(int i=0;i<m_n-1;i++)
		for(int j=i+1;j<m_n-1;j++)
			if(matvois[i][j])
			{
				m_m++;
				listvois[i].push_back(j);
				listvois[j].push_back(i);
			}
	m_n=m_n-1;
}



void Graphe::delVoisSommet(int x)
{
	bool tab[m_n];
	int nbdelsom = listvois[x].size();
	for(int i=0;i<m_n;i++)
		tab[i]=1;
	for(int i=0;i<nbdelsom;i++)
		tab[listvois[x][i]]=0;
	int newmat[m_n-nbdelsom][m_n-nbdelsom];
/*	vector< vector< bool> > newmat;
	newmat.resize(m_n-nbdelsom);
	for(int i=0;i<m_n-nbdelsom;i++)
	{
		vector<bool> add;
		newmat[i]=add;
		newmat[i].resize(m_n-nbdelsom);
	}*/
	int i1=0;
	int j1=0;
	for(int i=0;i<m_n;i++)
	{
		if(tab[i])
		{

			for(int j=0;j<m_n;j++)
				if(tab[j])
				{
					newmat[i1][j1]=matvois[i][j];
					j1++;
				}
			i1++;
			j1=0;
		}
	}
	matvois.resize(m_n-nbdelsom);
	for(int i=0;i<m_n;i++)
	{
		vector<bool> add;
		matvois[i]=add;
		matvois[i].resize(m_n-nbdelsom);
	}
  	for(int i=0;i<m_n-nbdelsom;i++)
    		for(int j=0;j<m_n-nbdelsom;j++)
      			matvois[i][j]=newmat[i][j];
	listvois.resize(m_n-nbdelsom);
	for(int i=0;i<m_n-nbdelsom;i++)
		listvois[i].clear();
	m_m=0;
	for(int i=0;i<m_n-nbdelsom;i++)
		for(int j=i+1;j<m_n-nbdelsom;j++)
			if(matvois[i][j])
			{
				m_m++;
				listvois[i].push_back(j);
				listvois[j].push_back(i);
			}
	m_n=m_n-nbdelsom;
}

void Graphe::addArete(int x,int y)
{
	if(!matvois[x][y])
	{
		m_m++;
		matvois[x][y]=true;
		matvois[y][x]=true;
		listvois[y].push_back(x);
		listvois[x].push_back(y);
	}
}

void Graphe::delArete(int x,int y)
{
	if(matvois[x][y])
	{
		m_m--;
		matvois[x][y]=false;
		matvois[y][x]=false;
		int i=0;
		while(i<listvois[x].size() && listvois[x][i]!=y)
			i++;
		if(i<listvois[x].size())
			listvois[x].erase(listvois[x].begin()+i);
		i=0;
		while(i<listvois[y].size() && listvois[y][i]!=x)
			i++;
		if(i<listvois[y].size())
			listvois[y].erase(listvois[y].begin()+i);
	}
}

int Graphe::nbSommet()
{
	return m_n;
}

int Graphe::nbArete()
{
	return m_m;
}

vector<int> Graphe::areteFirst()
{
	vector<int> rez;
	if(m_m!=0)
	{
		int i=0;
		while( i<(m_n*m_n) && !matvois[i/m_n][i%m_n])
		{
			i++;
		}
		if(i<m_n*m_n)
		{
			rez.push_back((int)i/m_n);
			rez.push_back((int)i%m_n);
			return rez;
		}
	}
	else
	{
		rez.push_back(-1);
		rez.push_back(-1);
		return rez;
	}
}

int Graphe::sommetMaxDeg()
{
	int rez=0;
	int taille=0;
	for(int i=0;i<m_n;i++)
	{
		if(taille<listvois[i].size())
		{
			rez=listvois[i].size();
			rez=i;
		}
	}
	return rez;
}

int Graphe::degSom(int x)
{
	return listvois[x].size();
}

void pointrandom(int n, int point[][4])
{
	int ecart = 360/n;

	for(int i=0;i<n;i++)
	{
		point[i][0]=int(cos((i*ecart)* PI / 180.0)*250+306);
		point[i][1]=int(sin((i*ecart)* PI / 180.0)*250+490);
		point[i][2]=int(cos((i*ecart)* PI / 180.0)*270+306);
		point[i][3]=int(sin((i*ecart)* PI / 180.0)*270+490);
	}
}

void Graphe::affichageGraphique(string fichier)       // Cree le fichier 'fichier'.ps qui affiche
{
	int point[m_n][4];
	pointrandom(m_n,point);
  	fstream output;
  	output.open(fichier.c_str(), ios::out);
  	if( output.fail() ) 
    	{
      		cerr << "ouverture en ecriture impossible" << endl;
      		exit(EXIT_FAILURE);
    	}
	output << "%!PS-Adobe-3.0" << endl;
	output << endl;  
	for(int i=0;i<m_n;i++)
	{
		output << point[i][0] << " " << point[i][1] << " 3 0 360 arc" <<endl;
   		output << "0 setgray" <<endl;
   		output << "fill" <<endl;
   		output << "stroke"<<endl;
   		output << endl;

		output << point[i][2] << " " << point[i][3] << " moveto"<<endl;
		output << "/Courier findfont 15 scalefont setfont" << endl;
    		output << "("<< i << ")" << " show" << endl;
    		output << "stroke" << endl;
    		output << endl;
   	}
 	output << endl;
 	for(int i=0;i<m_n;i++)
		for(int j=i+1;j<m_n;j++)
			if(matvois[i][j])
			{
				output  << point[i][0] << " " << point[i][1] << " moveto" << endl;
   				output << point[j][0] << " " << point[j][1] << " lineto" << endl;
   				output << "stroke" << endl;
   				output << endl;
   			}
}


void Graphe::toString()
{
cout<<endl<<"nb arete: "<<m_m<<" nb sommet: "<<m_n<<endl;

	for(int i=0;i<m_n;i++)
	{
		for(int j=0;j<m_n;j++)
			cout<<matvois[i][j]<<" ";
		cout<<endl;
	}
/*  for(int i=0;i<m_n;i++)
  {
    cout<<"voisin de "<<i<<" : ";
    for(int j=0;j<listvois[i].size();j++)
      cout<<listvois[i][j]<<" ; ";
    cout<<endl;
  }*/
}
;
