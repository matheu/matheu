#include "ImageBase.h"
#include <stdio.h>
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250];
    int numero;
	if (argc != 2)
	{
		printf("Usage: ImageIn.pgm l/c n�l/c \n");
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;


	//ImageBase imIn, imOut;
	ImageBase imIn;
	imIn.load(cNomImgLue);

	//ImageBase imG(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
 /*   if(l_c=='l')
    {
        if(0<numero && numero<imIn.getHeight())
        {
            for(int i=0;i<imIn.getWidth();i++)
            {
                printf("%d %d\n",i,imIn[numero][i]);
            }
        }
    }
    else
    {
        if(0<numero && numero<imIn.getWidth())
        {
            for(int i=0;i<imIn.getHeight();i++)
            {
                printf("%d %d\n",i,imIn[i][numero]);
            }
        }
    }
*/
    int r[256]{0},g[256]{0},b[256]{0};
	for(int y = 0; y < imIn.getHeight(); ++y)
		for(int x = 0; x < imIn.getWidth(); ++x)
		{
			r[imIn[y*3+2][x*3+0]]++; // R
			g[imIn[y*3+2][x*3+1]]++; // G
			b[imIn[y*3+2][x*3+2]]++; // B
		}
    for(int i=0;i<256;i++)
        printf("%d %d %d %d\n",i,r[i],g[i],b[i]);

	///////////////////////////////////////// Exemple de cr�ation d'une image couleur
	ImageBase imC(50, 100, true);

	for(int y = 0; y < imC.getHeight(); ++y)
		for(int x = 0; x < imC.getWidth(); ++x)
		{
			imC[y*3][x*3+0] = 200; // R
			imC[y*3][x*3+1] = 0; // G
			imC[y*3][x*3+2] = 0; // B
		}

	imC.save("imC.ppm");




	///////////////////////////////////////// Exemple de cr�ation d'une image en niveau de gris
	ImageBase imG(50, 100, false);

	for(int y = 0; y < imG.getHeight(); ++y)
		for(int x = 0; x < imG.getWidth(); ++x)
			imG[y][x] = 50;

	imG.save("imG.pgm");




	ImageBase imC2, imG2;

	///////////////////////////////////////// Exemple lecture image couleur
	imC2.load("imC.ppm");
	///////////////////////////////////////// Exemple lecture image en niveau de gris
	imG2.load("imG.pgm");



	///////////////////////////////////////// Exemple de r�cup�ration d'un plan de l'image
	ImageBase *R = imC2.getPlan(ImageBase::PLAN_R);
	R->save("R.pgm");
	delete R;



	return 0;
}
