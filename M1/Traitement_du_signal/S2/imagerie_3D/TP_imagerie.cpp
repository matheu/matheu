#include<stdio.h>
#include<stdlib.h>
#include<math.h>

using namespace std;

int main(int argc,char **argv)
{
	char nomImg[250],imgSorti[250];
	int dimX;
	int dimY;
	int dimZ;
	if(argc!=6)
	{
		printf("./tp <nomdufichier> <dimX> <dimY> <dimZ> <nomresultat> \n");
		exit(EXIT_FAILURE);
	}
	sscanf (argv[1],"%s",nomImg) ;
	sscanf (argv[2],"%d",&dimX) ;
	sscanf (argv[3],"%d",&dimY) ;
	sscanf (argv[4],"%d",&dimZ) ;
	sscanf (argv[5],"%s",imgSorti) ;

	FILE *f_image;
	if( (f_image = fopen(nomImg, "rb")) == NULL)
	{
		printf("\nPas d'acces en ecriture sur l'image %s \n", nomImg);
		exit(EXIT_FAILURE);
	}
	else
	{
		printf("\nErreur d'ecriture de l'image %s \n", nomImg);
		exit(EXIT_FAILURE);
	}

	fseek (f_image, 0, SEEK_END);
	long lSize = ftell (f_image);
	rewind (f_image);

	char * buffer = (char*) malloc (sizeof(char)*lSize);
	fread (buffer,1,lSize,f_image);

	unsigned short tabDonnees[dimX][dimY][dimZ];

	for(int x=0;x<dimX;x++)
		for(int y=0;y<dimY;y++)
			for(int z=0;z<dimZ;z++)
				tabDonnees[x][y][z]=buffer[x*dimX*dimY+y*dimY+z];


	fclose (f_image);
	free (buffer);
}

int getValue(unsigned short*** tabDonnees, int i, int j, int k)
{

}
