#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
#include "Wave.hpp"
#include <cmath>

#define PI 3.14159265
#define LA 440.0
#define DO 528.0
#define SI 495.0
#define MI 330.0
#define RE 297.0
#define FA 352.0
#define SOL 396.0

char fileName[200]; // Fichier wave

//==============================================================================
void printUsage(char* name) {

  cout << "\nUsage : "
       <<name
       <<" fichier.wav\n";
  exit(-1);
}

//==============================================================================
//	ak = somme pour n de 0 a N-1 (xn cos(2PI (k n) / N))
//	bk = - somme pour n de 0 a N-1 (xn sin(2PI (k n) / N))
void DFT(unsigned char *signal, double *partie_reelle, double *partie_imaginaire, int N)
{
	for(int i=0;i<N;i++)
	{
		partie_reelle[i]=0.0;
		partie_imaginaire[i]=0.0;
		for(int j=0;j<N;j++)
		{

			partie_reelle[i] += ( ((double)signal[j] - 125.0) * cos(2.0*PI*( ((double)i*(double)j)/(double)N) ) );
			partie_imaginaire[i] += ( ((double)signal[j] - 125.0) * sin(2.0*PI*(((double)i*(double)j)/(double)N)) );
		}
		partie_imaginaire[i] *=-1.0;
		
//	cout<<i<<" = parti_reel: "<<partie_reelle[i]<<" ; parti_imag: "<<partie_imaginaire[i]<<" ;somme : "<<floor(partie_reelle[i] + partie_imaginaire[i])<<endl;
	}
}

//==============================================================================
//  xn = somme pour k de 0 a N-1 (ak cos(2PI (kn)/N) - bk sin(2PI (kn)/N))
void IDFT(unsigned char *signal, double *partie_reelle, double *partie_imaginaire, int N)
{
	double* rez = new double[N];
	for(int i=0;i<N;i++)
	{
		rez[i]=0;
		for(int j=0;j<N;j++)
		{
			rez[i] += partie_reelle[j] * cos(2.0*PI*(((double)j*(double)i)/(double)N));
			rez[i] -= partie_imaginaire[j] * sin(2.0*PI*(((double)j*(double)i)/(double)N));
		}
		signal[i] = floor((rez[i]/N)+127.0);
	}
}
//==============================================================================
void affichageFourrier(unsigned char *signal, double *partie_reelle, double *partie_imaginaire, int N)
{
	double signal_d[N];
	double min = 10000000000000000.0;
	double max = -10000000000000000.0;
	for(int i=0;i<N;i++)
	{
		signal_d[i] = 0;
		signal_d[i]= sqrt(partie_reelle[i]*partie_reelle[i] + partie_imaginaire[i]*partie_imaginaire[i]);
		if(signal_d[i] > max) {
			max = signal_d[i];
		}
		if(signal_d[i] < min) {
			min =signal_d[i];
		}
	}
	for(int i=0; i<N; i++){
		signal[i] = (char)(((signal_d[i]-min)/(max-min))*255);
	}
}
//==============================================================================

void transform(double* partie_reelle, double* partie_imaginaire, int N,int fe, int frequence_suppr) {
  	int k= frequence_suppr*N/fe;
	int i=k;
  //	for(int i = k-10;i<k+10;i++)
	{
		partie_reelle[i] = 0.0;
		partie_imaginaire[i] = 0.0;
		partie_reelle[N-i] = 0.0;
		partie_imaginaire[N-i] = 0.0;
	}
}

//==============================================================================
void processOptionsInLine(int argc, char** argv){  
  if (argc != 2) {
    printUsage(argv[0]);
  }

  strcpy(fileName, argv[1]);  
}

int main(int argc, char *argv[]) {

  processOptionsInLine(argc, argv);

//Wave* wv = new Wave();
// Ouverture et ecriture d'un fichier
//(*wv).read("./BrokenGlass.wav");
//(*wv).write(fileName);

// Lire et obtenir les donn�es
/*
Wave* wv = new Wave();
(*wv).read("./GammePiano.wav");
int size;
unsigned char* data8;
(*wv).getData8(&data8, &size);
cout<<size<<"\n";
double frequence_echan = (*wv).getFrequence();
*/



int temps = 1;
double frequence_echan = 22000.0;
int size = frequence_echan*temps;
unsigned char* data8 = new unsigned char[size];
for(int i = 0; i < size/7; i++) {
data8[i] = floor(127*(1+sin((2.0*PI*(double)i)/(frequence_echan/LA))));
}

for(int i = size/7+1; i < 2*size/7; i++) {
data8[i] = floor(127*(1+sin((2.0*PI*(double)i)/(frequence_echan/SI))));
}
for(int i = 2*size/7+1; i < 3*size/7; i++) {
data8[i] = floor(127*(1+sin((2.0*PI*(double)i)/(frequence_echan/DO))));
}
for(int i = 3*size/7+1; i < 4*size/7; i++) {
data8[i] = floor(127*(1+sin((2.0*PI*(double)i)/(frequence_echan/RE))));
}
for(int i = 4*size/7+1; i < 5*size/7; i++) {
data8[i] = floor(127*(1+sin((2.0*PI*(double)i)/(frequence_echan/MI))));
}
for(int i = 5*size/7+1; i < 6*size/7; i++) {
data8[i] = floor(127*(1+sin((2.0*PI*(double)i)/(frequence_echan/FA))));
}
for(int i = 6*size/7+1; i < 7*size/7; i++) {
data8[i] = floor(127*(1+sin((2.0*PI*(double)i)/(frequence_echan/SOL))));
}


Wave* wv = new Wave(data8,size,1,frequence_echan);
(*wv).write(fileName);


double* partie_r = new double[size];
double* partie_i = new double[size];
cout<<"DFT"<<endl;
DFT(data8, partie_r, partie_i, size);
affichageFourrier(data8, partie_r, partie_i, size);
Wave* wv3 = new Wave(data8,size,1,frequence_echan);
(*wv3).write("DTF");


// Ici transform
cout<<"Transform"<<endl;
transform(partie_r, partie_i, size,frequence_echan, LA);
affichageFourrier(data8, partie_r, partie_i, size);
Wave* wv4 = new Wave(data8,size,1,frequence_echan);
(*wv4).write("DTFaprestransform");


cout<<"IDTF"<<endl;
IDFT(data8, partie_r, partie_i, size);
char* fileName2;
fileName2 = fileName;
fileName2[0] = '_';
Wave* wv2 = new Wave(data8,size,1,frequence_echan);
(*wv2).write(fileName2);
cout<<"fin"<<endl;
}

