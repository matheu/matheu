var diag;

document.onmousedown=function(event)
{
	var coordonnees = mouseCoord(document.getElementById("cPert"),event)
	diag.clic(coordonnees.x,coordonnees.y);
}

lancerPert=function()
{
	diag=new Diagramme();
	diag.dessine();
	
	$("#addTache").click(function(){
		diag.modeAjouterTache();
	});
	
		$("#addEtape").click(function(){
		diag.modeAjouterEtape();
	});
}

this.mouseCoord=function(canvas,event)
 {
	var element=canvas,
	ox = element.scrollLeft - element.offsetLeft,
	oy = element.scrollTop - element.offsetTop;
	while(element=element.offsetParent)
	{
		ox += element.scrollLeft - element.offsetLeft;
		oy += element.scrollTop - element.offsetTop;
	}
/*	if(!isForMovingMap)
	{
		ox -= deplacementMap.x;
		oy -= deplacementMap.y;
	}*/
	return {x:(event.clientX+ox),y:(event.clientY+oy)};
 }