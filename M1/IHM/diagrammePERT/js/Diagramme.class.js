function Diagramme()
{
	this.taches=Array();
	this.etapes=Array();
	this.canvas=document.getElementById("cPert");
	this.context=this.canvas.getContext("2d");
	this.context.fillStyle="rgba(120, 160, 255, 0.3)";
	this.rayonEtape=32;
	this.tailleFleche=6;
	this.lastIdTache=0;
	var deb = new Etape(0);
	var fin = new Etape(1);
	this.creationTache(deb,fin);
	this.lastIdEtape=2;
	this.mem= deb;
	this.memoriser=false;
	this.modeEtape = false;
	this.modeTache = false;
	this.hauteur = Array();
	this.hauteurdone = Array();
	this.etapes.push(deb);
	this.etapes.push(fin);
}

Diagramme.prototype.dessine=function()
{

	this.context.clearRect(0,0,this.canvas.width,this.canvas.height);
	
	// Calcul de la profondeur maximale
	var profondeurMax=0;
	for(var i=0; i<this.taches.length; i++)
	{
		
		//console.log(this.taches[i].suivE.niveau);
		if(this.taches[i].suivE!=null)
		{
			if(this.taches[i].suivE.niveau>profondeurMax)
				profondeurMax=this.taches[i].suivE.niveau;
		}
	}
	this.hauteur = Array();
	this.hauteurdone = Array();
	for(var i=0; i<=profondeurMax; i++)
	{
		this.hauteur[i]=0;
		this.hauteurdone[i]=0;
	}
	for(var i=0; i<this.etapes.length; i++)
	{
		
		if(this.etapes[i]!=null)
		{
			this.hauteur[this.etapes[i].niveau]=this.hauteur[this.etapes[i].niveau]+1;
		}
	}
	for(var i=0; i<this.etapes.length; i++)
	{
		if(this.etapes[i]!=null)
		{
			var x=this.canvas.width*((this.etapes[i].niveau+0.5)/(profondeurMax+1)) - this.rayonEtape*0.5;
			var y=((this.canvas.height/(this.hauteur[this.etapes[i].niveau]+1))*((this.hauteurdone[this.etapes[i].niveau])+1))-this.rayonEtape;
			this.hauteurdone[this.etapes[i].niveau]=this.hauteurdone[this.etapes[i].niveau]+1;
			this.etapes[i].x=x;
			this.etapes[i].y=y;
			this.context.beginPath();
			this.context.arc(
				x,
				y,
				this.rayonEtape,
				0,
				Math.PI*2
			);
			this.context.fill();
		}

	}

	for(var i=0; i<this.taches.length; i++)
	{
		// Titre de la tache
		this.context.save();
		this.context.fillStyle="#000";
		this.context.fillText(this.taches[i].nom, (this.taches[i].precE.x+this.taches[i].suivE.x)*0.5-20, (this.taches[i].precE.y+this.taches[i].suivE.y)*0.5+18);
		this.context.restore();
		
		// Dessin de la tache (c'est une fleche)
		this.context.save();
		this.context.fillStyle="#000";
		this.context.beginPath();
		this.context.moveTo(this.taches[i].precE.x+this.rayonEtape, this.taches[i].precE.y);
		this.context.lineTo(this.taches[i].suivE.x-this.rayonEtape, this.taches[i].suivE.y);
		this.context.lineTo(this.taches[i].suivE.x-this.rayonEtape-this.tailleFleche, this.taches[i].suivE.y-this.tailleFleche);
		this.context.moveTo(this.taches[i].suivE.x-this.rayonEtape, this.taches[i].suivE.y);
		this.context.lineTo(this.taches[i].suivE.x-this.rayonEtape-this.tailleFleche, this.taches[i].suivE.y+this.tailleFleche);
		this.context.stroke();
		this.context.restore();
	}
}


Diagramme.prototype.creationEtape=function(precE,suivE)
{
	if(precE.niveau<=suivE.niveau || (suivE.niveau==0 && suivE.id !=0))
	{
		var e = new Etape(this.lastIdEtape);
		this.etapes.push(e);
		this.lastIdEtape++;
		var i=0;
		while(i<suivE.precT.length && suivE.precT[i].precE.id!=precE.id)
		{
			i++;
		}
		if(i<precE.suivT.length)
		{
			suivE.precT[i].suivE = e;
			e.ajouterPrec(suivE.precT[i]);
			
			suivE.precT.remove(i);
		}
		else
		{
			var t1 = new Tache(precE,e,this.lastIdTache);this.lastIdTache++;
			this.taches.push(t1);
		}
		var t2 = new Tache(e,suivE,this.lastIdTache);this.lastIdTache++;
		this.taches.push(t2);
	}
}

Diagramme.prototype.clicCercle=function(xc,yc,x1,y1)
{
	var rez = ((this.rayonEtape*this.rayonEtape)>=((xc-x1)*(xc-x1)+(yc-y1)*(yc-y1)));
	return rez;
}


Diagramme.prototype.clic=function(x,y)
{
	var i=0;
	while(i<this.etapes.length && !this.clicCercle(this.etapes[i].x,this.etapes[i].y,x,y))
		i++;
	if(i<this.etapes.length)
	{
		if(this.memoriser)
		{
			if(this.modeEtape)
			{
				this.creationEtape(this.mem,this.etapes[i]);
				this.memoriser = false;
				this.modeEtape = false;
			}
			if(this.modeTache)
			{
				this.creationTache(this.mem,this.etapes[i]);
				this.memoriser = false;
				this.modeTache = false;
			}
		}
		else
		{
			this.mem = this.etapes[i];
			this.memoriser = true;
		}
	}
	else
		this.memoriser = false;
	this.dessine();
}

Diagramme.prototype.modeAjouterTache=function()
{
	this.modeEtape = false;
	this.modeTache = true;
	this.memoriser = false;
}

Diagramme.prototype.modeAjouterEtape=function()
{
	this.modeEtape = true;
	this.modeTache = false;
	this.memoriser = false;
}

Diagramme.prototype.creationTache=function(precE,suivE)
{
	if(precE.niveau<=suivE.niveau || (suivE.niveau==0 && suivE.id !=0))
	{
		var i=0;
		while(i<suivE.precT.length && !suivE.precT[i].precE.id==precE.id)
		{
			i++;
		}
		if(i>=precE.suivT.length)
		{
			var t1 = new Tache(precE,suivE,this.lastIdTache);this.lastIdTache++;
			this.taches.push(t1);
		}
	}
}
