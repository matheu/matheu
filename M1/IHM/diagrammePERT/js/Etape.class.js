function Etape(id)
{
	this.id=id;
	this.x=0;
	this.y=0;
	this.niveau = 0;
	this.deadlineVoulu =0; //temps en jours
	this.deadline = 0; //temps en jours
	this.suivT = Array(); //liste des taches suivantes
	this.precT = Array(); //liste des taches precedente
}

Etape.prototype.ajouterSuiv=function(tache)
{
	if(tache.suivE.niveau<this.niveau+1)
		tache.suivE.majNiveau();
	this.suivT.push(tache);
}

Etape.prototype.ajouterPrec=function(tache)
{
	this.precT.push(tache);
	this.majDeadline();
	this.majNiveau();
}

Etape.prototype.majDeadline=function()
{
	for(var i=0; i<this.precT.length; i++)
	{
		if(this.precT[i].precE.deadline + this.precT[i].temps > this.deadline)
			this.deadline = this.precT[i].precE.deadline + this.precT[i].temps;
	}
	for(var i=0; i<this.suivT.length; i++)
	{
		this.suivT[i].suivE.majDeadline();
	}
}

Etape.prototype.majNiveau=function()
{
	for(var i=0; i<this.precT.length; i++)
	{
		if( (this.precT[i].precE.niveau + 1) > this.niveau)
			this.niveau = this.precT[i].precE.niveau + 1;
	}
	for(var i=0; i<this.suivT.length; i++)
	{
		this.suivT[i].suivE.majNiveau();
	}
}
