/**
 * AbsCon - Copyright (c) 2000-2013, CRIL-CNRS - lecoutre@cril.fr
 * 
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the terms of the CONTRAT DE LICENCE DE LOGICIEL
 * LIBRE CeCILL which accompanies this distribution, and is available at http://www.cecill.info
 */

import static constraints.hard.intension.Expr.add;
import static constraints.hard.intension.Expr.eq;
import static constraints.hard.intension.Expr.ge;
import static constraints.hard.intension.Expr.le;
import problem.Problem;
import variables.Variable;

public class Example extends Problem {

	private Variable w, x, y, z;

	@Override
	protected void specifyVariables() {
		w = addVariableInteger("w", 1, 3);
		x = addVariableInteger("x", 1, 3);
		y = addVariableInteger("y", 1, 4);
		z = addVariableInteger("z", 1, 4);
	}

	@Override
	protected void specifyConstraints() {
		addConstraintIntension(eq(x, y));
		addConstraintIntension(le(x, add(y, 1)));
		addConstraintIntension(ge(y, add(w, z)));
		addConstraintTableSupports(new Variable[] { x, z }, new int[][] { { 1, 2 }, { 2, 1 }, { 2, 4 }, { 3, 3 } });
	}
}
