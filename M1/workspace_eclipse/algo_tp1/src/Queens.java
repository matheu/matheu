/**
 * AbsCon - Copyright (c) 2000-2013, CRIL-CNRS - lecoutre@cril.fr
 * 
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the terms of the CONTRAT DE LICENCE DE LOGICIEL
 * LIBRE CeCILL which accompanies this distribution, and is available at http://www.cecill.info
 */

import static constraints.hard.intension.Expr.abs;
import static constraints.hard.intension.Expr.add;
import static constraints.hard.intension.Expr.and;
import static constraints.hard.intension.Expr.eq;
import static constraints.hard.intension.Expr.ne;
import static constraints.hard.intension.Expr.sub;
import problem.Problem;
import tools.Kit;
import constraints.hard.global.GlobalCardinalityConstant;

public class Queens extends Problem {

	private int nbQueens, model;

	@Override
	protected void specifyParameters() {
		nbQueens = addParameterInt("Nb queens", 2); // message, valeurMin
		model = addParameterInt("Model (1 to 5)", 1, 5); // message, valeurMin, valeurMax
	}

	@Override
	protected void specifyVariables() {
		addVariableIntegerArray("Q", 0, nbQueens - 1, nbQueens);
		if (model == 5) {
			for (int i = 0; i < nbQueens; i++)
				addVariableInteger("R" + i, 0 + i, nbQueens - 1 + i);
			for (int i = 0; i < nbQueens; i++)
				addVariableInteger("S" + i, 0 - i, nbQueens - 1 - i);
		}
	}

	private String onDifferentDiagonal(int i, int j) {
		int rowDistance = Math.abs(i - j);
		String colDistance = abs(sub(variables[i], variables[j]));
		return ne(colDistance, rowDistance);
	}

	private void model1() {
		for (int i = 0; i < nbQueens - 1; i++)
			for (int j = i + 1; j < nbQueens; j++)
				addConstraintIntension(ne(variables[i], variables[j]));
		for (int i = 0; i < nbQueens - 1; i++)
			for (int j = i + 1; j < nbQueens; j++)
				addConstraintIntension(onDifferentDiagonal(i, j));
	}

	private void model2() {
		for (int i = 0; i < nbQueens - 1; i++)
			for (int j = i + 1; j < nbQueens; j++)
				addConstraintIntension(and(ne(variables[i], variables[j]), onDifferentDiagonal(i, j)));
	}

	private void model3() {
		addConstraintAllDifferent(variables);
		for (int i = 0; i < nbQueens - 1; i++)
			for (int j = i + 1; j < nbQueens; j++)
				addConstraintIntension(and(ne(variables[i], variables[j]), onDifferentDiagonal(i, j)));
	}

	private void model4() {
		addConstraint(new GlobalCardinalityConstant(this, variables, Kit.buildIntArray(nbQueens, 0), Kit.buildIntArrayWithUniqueValue(nbQueens, 1),
				Kit.buildIntArrayWithUniqueValue(nbQueens, 1)));
		for (int i = 0; i < nbQueens - 1; i++)
			for (int j = i + 1; j < nbQueens; j++)
				addConstraintIntension(and(ne(variables[i], variables[j]), onDifferentDiagonal(i, j)));
	}

	private void model5() {
		addConstraintAllDifferent(getVariablesOfArray(variables,0, nbQueens));
		addConstraintAllDifferent(getVariablesOfArray(variables,nbQueens, nbQueens));
		addConstraintAllDifferent(getVariablesOfArray(variables,nbQueens * 2, nbQueens));
		for (int i = 0; i < nbQueens; i++)
			addConstraintIntension(eq(add(variables[i], i), variables[i + nbQueens]));
		for (int i = 0; i < nbQueens; i++)
			addConstraintIntension(eq(sub(variables[i], i), variables[i + nbQueens * 2]));
	}

	@Override
	protected void specifyConstraints() {
		// At that point, all posted variables are available in the array called variables
		switch (model) {
		case 1:
			model1();
			break;
		case 2:
			model2();
			break;
		case 3:
			model3();
			break;
		case 4:
			model4();
			break;
		case 5:
			model5();
			break;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public void prettySolutionDisplay () {
	for (int i = 0; i < variables . length ; i ++) {
	for (int j = 0; j < nbQueens ; j ++)
	System . out . print ( ( variables [i]). getDomain (). getUniqueValue () ==j?"Q":"-");
	System . out . println ();
	}
	}

	@Override
	public void specifySolutionDisplay() {
		for (int i = 0; i < nbQueens; i++) {
			for (int j = 0; j < nbQueens; j++)
				Kit.pr(variables[i].domain.getUniqueValue() == j ? "Q " : "- ");
			Kit.prn();
		}
	}
}
