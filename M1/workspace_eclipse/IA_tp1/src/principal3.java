import java.io.IOException;

import structure.KnowledgeBase;


public class principal3 {

	public static void main(String[] args) {
		KnowledgeBase kb;
		try {
//			kb = new KnowledgeBase("essai1.txt");
//			kb = new KnowledgeBase("essai2.txt");
			kb = new KnowledgeBase("essai3.txt");
			System.out.println(kb.toString());
			kb.instanciation();
			kb.ForwardChaining();
			System.out.println(kb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
