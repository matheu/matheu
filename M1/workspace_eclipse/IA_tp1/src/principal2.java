import java.io.IOException;

import structure.KnowledgeBase;

public class principal2 {

	public static void main(String[] args) {
		KnowledgeBase kb;
		try {
			kb = new KnowledgeBase("essai.txt");
			System.out.println(kb.toString());
			kb.ForwardChaining();
			System.out.println(kb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
