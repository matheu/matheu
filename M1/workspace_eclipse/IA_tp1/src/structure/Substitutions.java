package structure;

import java.util.ArrayList;

public class Substitutions 
{
	ArrayList<Substitution> s;
	ArrayList<Term> E1;//constante
	ArrayList<Term> E2;//variable
	
	public Substitutions(ArrayList<Term> cons, ArrayList<Term> var) 
	{
		s=new ArrayList<Substitution>();
		E1 = cons;
		E2 = var;
	}
	
	
	public void generateAllSubstitutions()
	{
		for(int i=0;i<E2.size();i++)
		{
			Substitution sub = new Substitution();
			for(int j=0;j<E1.size();j++)
			{
				sub.addCoupleTerms(new CoupleTerms(E2.get(i), E1.get(j)));
			}
			s.add(sub);
		}
	}
	
	public int size()
	{
		return s.size();
	}

	public Substitution get(int i) 
	{
		return s.get(i);
	}
	
	
}
