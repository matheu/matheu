package structure;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class KnowledgeBase {
	FactBase fb;
	RuleBase rb;

	public KnowledgeBase() {
		fb = new FactBase();
		rb = new RuleBase();
	}
	
	private void remplirConstante(ArrayList<Term> constante)
	{
		for(int i=0;i<fb.getTerms().size();i++)
			constante.add(fb.getTerms().get(i));
		for(int i=0;i<rb.size();i++)
		{
			for(int j=0;j<rb.getRule(i).getHypothesis().size();j++)
			{
				for(int k=0;k<rb.getRule(i).getAtomHyp(j).getArity();k++)
				{
					if(rb.getRule(i).getAtomHyp(j).getArgI(k).isConstant())
					{
						int t=0;
						while(t<constante.size() && !constante.get(t).equalsT(rb.getRule(i).getAtomHyp(j).getArgI(k)))
							t++;
						if (t==constante.size())		
							constante.add(rb.getRule(i).getAtomHyp(j).getArgI(k));
					}
				}
			}
			for(int k=0;k<rb.getRule(i).getConclusion().getArity();k++)
			{
				if(rb.getRule(i).getConclusion().getArgI(k).isConstant())
				{
					int t=0;
					while(t<constante.size() && !constante.get(t).equalsT(rb.getRule(i).getConclusion().getArgI(k)))
						t++;
					if (t==constante.size())		
						constante.add(rb.getRule(i).getConclusion().getArgI(k));
				}
			}
		}
	}
	
	public void instanciation()
	{
		ArrayList<Integer> arity = new ArrayList<Integer>();
		ArrayList<Term> constante = new ArrayList<Term>();
		remplirConstante(constante);
		ArrayList<Term> variable = new ArrayList<Term>();
		remplirVariable(variable,arity);
		for(int i=0;i<variable.size();i++)
			System.out.println(variable.get(i).getLabel());
		Substitutions subs = new Substitutions(constante, variable);
		subs.generateAllSubstitutions();
		
		for(int i=0;i<subs.size();i++)
		{
			int n=0;
			while(n<rb.size())
			{
				boolean end=false;
				int k=0;
				while(!end && k<rb.getRule(n).getHypothesis().size())
				{
					int l=0;
					while(!end && l<rb.getRule(n).getHypothesis().get(k).getArity())
					{

						if(rb.getRule(n).getHypothesis().get(k).getArgI(l).equalsT(subs.get(i).getI(0).getT1()))
						{
							for(int j=0;j<subs.get(i).size();j++)
							{
								Rule rbnew = new Rule(rb.getRule(n));
								rbnew.getHypothesis().get(k).setArgI(l, subs.get(i).getI(j).getT2());
								rb.addRule(rbnew);
								for(int x=0;x<rbnew.getHypothesis().size();x++)
									for(int y=0;y<rbnew.getHypothesis().get(x).getArity();y++)
										if(rbnew.getHypothesis().get(x).getArgI(y).equalsT(subs.get(i).getI(j).getT1()))
											rbnew.getHypothesis().get(x).setArgI(y, subs.get(i).getI(j).getT2());
								for(int z=0;z<rbnew.getConclusion().getArity();z++)
									if(rbnew.getConclusion().getArgI(z).equalsT(subs.get(i).getI(j).getT1()))
										rbnew.getConclusion().setArgI(z,subs.get(i).getI(j).getT2());
								
							}
							end=true;
							rb.delRule(n);
						}
						l++;
					}
					k++;
				}
				if(!end)
					n++;
				end=false;
			}
		}
	}

	
	private void remplirVariable(ArrayList<Term> variable,ArrayList<Integer> ari) 
	{
		for(int i=0;i<rb.size();i++)
		{
			ari.add(0);
			for(int j=0;j<rb.getRule(i).getHypothesis().size();j++)
			{
				for(int k=0;k<rb.getRule(i).getAtomHyp(j).getArity();k++)
				{
					if(rb.getRule(i).getAtomHyp(j).getArgI(k).isVariable())
					{
						//rb.getRule(i).getAtomHyp(j).setArgI(k, new Term(rb.getRule(i).getAtomHyp(j).getArgI(k).getLabel()+i, false));
						int t=0;
						while(t<variable.size() && !variable.get(t).equalsT(rb.getRule(i).getAtomHyp(j).getArgI(k)))
							t++;
						if (t==variable.size())
						{
							ari.set(i, ari.get(i)+1);
							variable.add(rb.getRule(i).getAtomHyp(j).getArgI(k));
						}
					}
				}
			}
			for(int k=0;k<rb.getRule(i).getConclusion().getArity();k++)
			{
				if(rb.getRule(i).getConclusion().getArgI(k).isVariable())
				{
					//rb.getRule(i).getConclusion().setArgI(k, new Term(rb.getRule(i).getConclusion().getArgI(k).getLabel()+i, false));
					int t=0;
					while(t<variable.size() && !variable.get(t).equalsT(rb.getRule(i).getConclusion().getArgI(k)))
						t++;
					if (t==variable.size())		
					{
						ari.set(i, ari.get(i)+1);
						variable.add(rb.getRule(i).getConclusion().getArgI(k));
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param nomtexte
	 * @throws IOException 
	 */
	public KnowledgeBase(String nomtexte) throws IOException {
		BufferedReader lectureFichier = new BufferedReader(new FileReader(
				nomtexte));
		String s = lectureFichier.readLine();
		// s = lectureFichier.readLine();
		try {
			int k = Integer.parseInt(s);
			s = lectureFichier.readLine();

			String rez = s;
			for (int i = 1; i < k; i++) {
				s = lectureFichier.readLine();
				rez = rez + ";" + s;
			}
			fb = new FactBase(rez);
			rb = new RuleBase();
			s = lectureFichier.readLine();
			int n = Integer.parseInt(s);
			for (int i = 0; i < n; i++) {
				s = lectureFichier.readLine();
				rb.addRule(new Rule(s));
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		lectureFichier.close();
	}

	public void ForwardChaining() {
		ArrayList<Atom> atraiter = new ArrayList<Atom>();
		atraiter.addAll(fb.getAtoms());
		int[] tab = new int[rb.size()];
		for (int i = 0; i < rb.size(); i++) {
			tab[i]=rb.getRule(i).getHypothesis().size();
		}
		while(!atraiter.isEmpty())
		{
			Atom traite = atraiter.get(0);
			atraiter.remove(0);
			for(int i =0;i<rb.size();i++)
			{
				for(int j=0;j<rb.getRule(i).getHypothesis().size();j++)
					if(rb.getRule(i).getHypothesis().get(j).equalsA(traite))
						tab[i]--;
				if(tab[i]==0)
				{
					int k=0;
					while(k<fb.size() && !fb.getAtoms().get(k).equalsA(rb.getRule(i).getConclusion()))
						k++;
					if(k>=fb.size())
					{
						fb.addAtom(rb.getRule(i).getConclusion());
						atraiter.add(rb.getRule(i).getConclusion());
					}
				}
			}
		}
	}
	
	/**
	 * Saturation de la base de fait avec la classe homomorphisme
	 */
	public void parHomomorphisme()
	{
		ArrayList<Term> constante = new ArrayList<Term>();
		remplirConstante(constante);
		boolean changement = false;
		while(!changement)
		{
			changement = true;
			for(int l=0;l<getRb().size();l++)
			{
				Homomorphismes h = new Homomorphismes(constante,getRb().getRule(l).getHypothesis(),getFb().getAtoms());
				h.genererHomomorphismes();
				for(int i=0;i<h.size();i++)
				{
					Atom a = new Atom(getRb().getRule(l).getConclusion());
					for(int j=0;j<a.getArity();j++)
					{
						int k=0;
						while(k<h.getHomomorphismes().get(i).size() && !a.getArgI(j).equalsT(h.getHomomorphismes().get(i).getI(k).getT1()))
							k++;
						if(k<h.getHomomorphismes().get(i).size())
							a.setArgI(j, h.getHomomorphismes().get(i).getI(k).getT2());
					}
					int j=0;
					while(j<getFb().size() && !getFb().getAtoms().get(j).equalsA(a))
						j++;
					if(j>=getFb().size())
					{
						changement=false;
						addFait(a);
					}
				}
			}
		}
	}

	public void addRegle(Rule regle) {
		rb.addRule(regle);
	}

	public void addFait(Atom fait) {
		fb.addAtom(fait);
	}

	@Override
	public String toString() {
		return ("Etat initial de la base de faits :\n" + fb
				+ "\nBase de regles :\n" + rb);
	}

	public FactBase getFb() {
		return fb;
	}

	public void setFb(FactBase fb) {
		this.fb = fb;
	}

	public RuleBase getRb() {
		return rb;
	}

	public void setRb(RuleBase rb) {
		this.rb = rb;
	}

}
