package structure;

public class CoupleTerms 
{
	Term t1;
	Term t2;
	
	public CoupleTerms(Term t1,Term t2)
	{
		this.t1=t1;
		this.t2=t2;
	}

	public Term getT1() {
		return t1;
	}

	public void setT1(Term t1) {
		this.t1 = t1;
	}

	public Term getT2() {
		return t2;
	}

	public void setT2(Term t2) {
		this.t2 = t2;
	}
	
	public String toString()
	{
		return t1.getLabel()+";"+t2.getLabel();
	}
}
