package structure;

import java.util.ArrayList;

public class Substitution 
{
	ArrayList<CoupleTerms> s;

	public Substitution(ArrayList<CoupleTerms> s) 
	{
		this.s = s;
	}
	
	public void remove(int i)
	{
		s.remove(i);
	}
	
	public void removeEnd()
	{
		remove(s.size()-1);
	}

	public Substitution() 
	{
		s = new ArrayList<CoupleTerms>();
	}
	
	public CoupleTerms getI(int i)
	{
		return s.get(i);
	}
	
	public void setsi(int i,CoupleTerms ct)
	{
		s.set(i, ct);
	}
	
	public void addCoupleTerms(CoupleTerms ct)
	{
		s.add(ct);
	}
	
	public int size()
	{
		return s.size();
	}
	
	public String toString()
	{
		String chaine = "";
		for(CoupleTerms c : s)
			chaine += c.toString()+" ";
		return chaine;
	}
}
