package structure;

import java.util.ArrayList;

public class Homomorphismes 
{
	private ArrayList<Atom> a1;
	private ArrayList<Atom> a2;
	private ArrayList<Integer> a1size; // Pour chaque atome de a1 on a son "rang" maximum (variable numerotee la plus elevee)
	ArrayList<Term> variable;
	ArrayList<Term> constante;
	private ArrayList<Substitution> hom;
	
	/**
	 * constructeur avec les 2 listes d'atomes recherche d'homomorphisme de a1 dans a2
	 * @param a1
	 * @param a2
	 */
	public Homomorphismes(ArrayList<Atom> a1,ArrayList<Atom> a2)
	{
		hom = new ArrayList<Substitution>();
		this.a1=a1;
		this.a2=a2;
		constante = new ArrayList<Term>();
		variable = new ArrayList<Term>();
		a1size = new ArrayList<Integer>();
	}
	
	/**
	 * Constructeur avec les 2 listes d'atomes recherche d'homomorphisme de a1 dans a2
	 * avec toute les constantes
	 * @param constante 
	 * @param a1 
	 * @param a2
	 */
	public Homomorphismes(ArrayList<Term> constante,ArrayList<Atom> a1,ArrayList<Atom> a2) 
	{
		hom = new ArrayList<Substitution>();
		this.a1=a1;
		this.a2=a2;
		this.constante = constante;
		variable = new ArrayList<Term>();
		a1size = new ArrayList<Integer>();
	}

	/**
	 * pour connaitre toute les constantes que l'on va traiter
	 */
	public void remplirConstante()
	{
		for(int i=0;i<a2.size();i++)
		{
			for(int j=0;j<a2.get(i).getArity();j++)
			{
				if(a2.get(i).getArgI(j).isConstant())
				{
					int t=0;
					while(t<constante.size() && !constante.get(t).equalsT(a2.get(i).getArgI(j)))
						t++;
					if (t==constante.size())
						constante.add(a2.get(i).getArgI(j));
					
				}
			}
		}
	}
	
	/**
	 * pour connaitre toute les variables que l'on va traiter
	 * Ainsi que connaitre le rang des atomes de a1
	 */
	private void remplirVariable() 
	{
		for(int i=0;i<a1.size();i++)
		{
			a1size.add(0);
			for(int j=0;j<a1.get(i).getArity();j++)
			{
				if(a1.get(i).getArgI(j).isVariable())
				{
					int t=0; // t represente le numero de la variable de "rang" maximum
					a1.get(i).setArgI(j, new Term(a1.get(i).getArgI(j).getLabel()/*+1*/, false));
					while(t<variable.size() && !variable.get(t).equalsT(a1.get(i).getArgI(j)))
						t++;
					if (t==variable.size())
						variable.add(a1.get(i).getArgI(j));
					
					if(t>a1size.get(i))
						a1size.set(i, t);
				}
			}
		}
	}
	
	/*
	 * Test d'homomorphisme partiel sur les atomes d'arite "prof" pour une substitution sub
	 */
	private boolean partielHomomorphisme(Substitution sub, int prof)
	{
		// On regarde tous les atomes de A1 qui ont autant de variables que la valeur de la profondeur
		for(int i=0;i<a1size.size();i++)
		{
			if(a1size.get(i)==prof)
			{
				// On cree un atome avec toutes ses variables affectees par la substitution
				Atom a = new Atom(a1.get(i));
				for(int j =0;j<a.getArity();j++)
				{
					int k=0;
					while(k<sub.size() && !a.getArgI(j).equalsT(sub.getI(k).getT1()))
						k++;
					if(k<sub.size())
						a.setArgI(j,sub.getI(k).getT2());
				}
				int j=0;
				while(j<a2.size() && !a2.get(j).equalsA(a))
					j++;
				// Si l'atome avec variables affectees n'apparait pas dans A2 alors on n'a pas d'homomorphisme partiel
				if(j>=a2.size())
					return false;
			}
		}
		return true;
	}
	
	/**
	 * Fonction qui enclenche la recherche d'homomorphisme
	 */
	public void genererHomomorphismes()
	{
		remplirVariable();
		
		Substitution s = new Substitution();
		if(variable.size()>0)
			generation(s,0,0);
		else
		{
			for(int i=0;i<a1.size();i++)
			{
					boolean good = true;
					int j=0;
					while(j<a2.size() && !a2.get(j).equalsA(a1.get(0)))
						j++;
					if(j>=a2.size())
						good=false;
					i++;
					if(good)
						hom.add(new Substitution());
			}
		}
			
			
	}
	
	/**
	 * fonction recursive pour la recherche des homomorphismes
	 * @param s ou l'on enregistre les affections des variables
	 * @param prof profondeur de l'arbre
	 * @param largeur largeur de notre noeud
	 */
	private void generation(Substitution s, int prof, int largeur) 
	{
		s.addCoupleTerms(new CoupleTerms(variable.get(prof), constante.get(largeur)));
		
		
		if(partielHomomorphisme(s,prof))
		{
			if(prof == variable.size()-1)
			{
				Substitution good = new Substitution();
				for(int i=0;i<s.size();i++)
					good.addCoupleTerms(s.getI(i));
				hom.add(good);
			}
			else
			{
				generation(s,prof+1,0);
			}
		}
		largeur++;
		if(largeur<constante.size())
		{
			s.removeEnd();
			generation(s, prof, largeur);
		}
		else
		{
			s.removeEnd();
		}
	}

	public ArrayList<Substitution> getHomomorphismes()
	{
		return hom;
	}
	
	public int size()
	{
		return hom.size();
	}
	
	public void printHomomorphisme()
	{
		System.out.println("Il y a "+hom.size()+" homomorphisme(s)");
		for(Substitution s : hom)
			System.out.println(s.toString());
	}
}
