import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import structure.Atom;
import structure.Homomorphismes;
import structure.KnowledgeBase;


public class Principale5
{
	static KnowledgeBase kb;
	public static void main(String[] args) throws IOException
	{
		// Jeu d'essai avec l'exemple du cours
		/*ArrayList<Atom> a1 = new ArrayList<Atom>();
		a1.add(new Atom("r(x,y,z)"));
		a1.add(new Atom("s(z,t)"));
		
		ArrayList<Atom> a2 = new ArrayList<Atom>();	
		a2.add(new Atom("r('a','b','c')"));
		a2.add(new Atom("s('b','a')"));
		a2.add(new Atom("s('c','b')"));*/
		
		// Jeu d'essai évaluation TP
//		kb = new KnowledgeBase("essai1.txt");
//		kb = new KnowledgeBase("essai2.txt");
//		kb = new KnowledgeBase("essai3.txt");
//		kb = new KnowledgeBase("essai4.txt");
		kb = new KnowledgeBase("essai5.txt");

		System.out.println(kb.toString());
		kb.parHomomorphisme();
		System.out.println(kb.toString());
		
		// Traitement requete
		requete();
		
		
		// Calcul du temps d'execution
//		long tempsDepart = System.currentTimeMillis();
//		System.out.println("Temps execution : "+(System.currentTimeMillis()-tempsDepart)+"ms");
	}
	
	@SuppressWarnings("resource")
	public static void requete()
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Veuillez saisir votre Requete :");
		String str = sc.nextLine();
		Atom r =new Atom(str);
		ArrayList<Atom> liste=new ArrayList<Atom>();
		liste.add(r);
		Homomorphismes hom = new Homomorphismes(liste, kb.getFb().getAtoms());
		hom.remplirConstante();
		hom.genererHomomorphismes();
		ArrayList<Atom> rez=new ArrayList<Atom>();
		for(int i=0;i<hom.size();i++)
		{
			Atom a = new Atom(r);
			for(int j=0;j<a.getArity();j++)
			{
				int k=0;
				while(k<hom.getHomomorphismes().get(i).size() && !a.getArgI(j).equalsT(hom.getHomomorphismes().get(i).getI(k).getT1()))
					k++;
				if(k<hom.getHomomorphismes().get(i).size())
					a.setArgI(j, hom.getHomomorphismes().get(i).getI(k).getT2());
			}
			rez.add(a);
		}
		System.out.println(r.toString());
		for(int i=0;i<rez.size();i++)
		{
			System.out.println(rez.get(i).toString());
		}
	}
}