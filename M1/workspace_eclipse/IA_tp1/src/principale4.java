import java.io.IOException;

import structure.KnowledgeBase;


public class principale4 {
	
	public static void main(String[] args) {
		KnowledgeBase kb;
		try {
			kb = new KnowledgeBase("essai4.txt");
			System.out.println(kb.toString());
			kb.instanciation();
			kb.ForwardChaining();
			System.out.println(kb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
