#include "Vecteur.h"
#include <cstdlib>
#include <iostream>
#include <math.h>

using namespace std;

Vecteur::Vecteur()
{
    _x=0;
    _y=0;
    _z=0;
}

Vecteur::Vecteur(double x, double y, double z)
{
    _x=x;
    _y=y;
    _z=z;
}

Vecteur::Vecteur(Vecteur &v)
{
    _x=v.getX();
    _y=v.getY();
    _z=v.getZ();
}

double Vecteur::norme()
{
    return sqrt((_x*_x)+(_y*_y)+(_z*_z));
}

void Vecteur::normalize()
{
    double a = norme();
    _x=_x/a;
    _y=_y/a;
    _z=_z/a;
}

double Vecteur::scalar(Vecteur vect2)
{
    return (vect2.getX()*_x)+(vect2.getY()*_y)+(vect2.getZ()*_z);
}

Vecteur* Vecteur::vectoriel(Vecteur vect2)
{
    Vecteur* v = new Vecteur((double)(_y*vect2.getZ()-_z*vect2.getY() ), (double)(_z*vect2.getX()-_x*vect2.getZ()) , (double)(_x*vect2.getY()-_y*vect2.getX()));
    return v;
}

double Vecteur::angle(Vecteur vect2)
{
    double cos = scalar(vect2)/(norme()*vect2.norme());
    return acos(cos);
}


void Vecteur::toString()
{
    cout<<"x:"<<_x<<" y:"<<_y<<" z:"<<_z<<endl;
}

double Vecteur::getX()
{
    return _x;
}
double Vecteur::getY()
{
    return _y;
}
double Vecteur::getZ()
{
    return _z;
}
void Vecteur::setX(double x)
{
    _x=x;
}
void Vecteur::setY(double y)
{
    _y=y;
}
void Vecteur::setZ(double z)
{
    _z=z;
}
;
