#include "Point.h"
#include <cstdlib>
#include <iostream>
#include <math.h>

using namespace std;

Point::Point()
{
    _x=0;
    _y=0;
    _z=0;
}

Point::Point(double x, double y, double z)
{
    _x=x;
    _y=y;
    _z=z;
}

Point::Point(Point &p)
{
    _x=p.getX();
    _y=p.getY();
    _z=p.getZ();
}

Point* Point::projectOnLine(Point b,Point c)
{
//    |BA’|=BA.BC/|BC|
//    A’x=Bx+Ux*|BA’|
    Point* p = new Point();
    Vecteur* bc = new Vecteur(c.getX()-b.getX(),c.getY()-b.getY(),c.getZ()-b.getZ());
    Vecteur* ba = new Vecteur(_x-b.getX(),_y-b.getY(),_z-b.getZ());
    bc->normalize();
    p->setX(b.getX()+bc->getX()*(ba->scalar(*bc)/bc->norme()));
    p->setY(b.getY()+bc->getY()*(ba->scalar(*bc)/bc->norme()));
    p->setZ(b.getZ()+bc->getZ()*(ba->scalar(*bc)/bc->norme()));
    return p;
}

Point* Point::projectOnLine(Point b,Vecteur bc)
{
    Point* p = new Point();
    Vecteur* ba = new Vecteur(_x-b.getX(),_y-b.getY(),_z-b.getZ());
    bc.normalize();
    p->setX(b.getX()+bc.getX()*(ba->scalar(bc)/bc.norme()));
    p->setY(b.getY()+bc.getY()*(ba->scalar(bc)/bc.norme()));
    p->setZ(b.getZ()+bc.getZ()*(ba->scalar(bc)/bc.norme()));
    return p;
}

Point* Point::projectOnPlan(Point a,Vecteur n)
{
//    |MM’|=MA.n/|n|
//    M’x =Mx-nx*|MM’|
    Point* p = new Point();
    Vecteur *ma = new Vecteur(a.getX()-_x,a.getY()-_y,a.getZ()-_z);
    p->setX(_x-n.getX()*(ma->scalar(n)/n.norme()));
    p->setY(_y-n.getY()*(ma->scalar(n)/n.norme()));
    p->setZ(_z-n.getZ()*(ma->scalar(n)/n.norme()));
    return p;
}

Vecteur* Point::toVecteur(Point p)
{
    Vecteur* v = new Vecteur(p.getX()-_x,p.getY()-_y,p.getZ()-_z);
    return v;
}

void Point::toString()
{
    cout<<"x:"<<_x<<" y:"<<_y<<" z:"<<_z<<endl;
}

double Point::getX()
{
    return _x;
}
double Point::getY()
{
    return _y;
}
double Point::getZ()
{
    return _z;
}
void Point::setX(double x)
{
    _x=x;
}
void Point::setY(double y)
{
    _y=y;
}
void Point::setZ(double z)
{
    _z=z;
}
;
