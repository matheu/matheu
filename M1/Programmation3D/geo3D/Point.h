
#include "./Vecteur.h"

class Point
{
	public:
	Point();
	Point(double x,double y ,double z);
	Point(Point &p);
	Point* projectOnLine(Point pt1,Point pt2);
	Point* projectOnLine(Point b,Vecteur bc);
	Point* projectOnPlan(Point a,Vecteur n);

    Vecteur* toVecteur(Point p);
    void toString();
    double getX();
    double getY();
    double getZ();
    void setX(double x);
    void setY(double y);
    void setZ(double z);

	private:
    double _x,_y,_z;

};
