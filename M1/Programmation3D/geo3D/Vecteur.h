

class Vecteur
{
	public:
	Vecteur();
	Vecteur(double x,double y,double z);
	Vecteur(Vecteur &v);

    double norme();
    void normalize();
    double scalar(Vecteur vect2);
    Vecteur* vectoriel(Vecteur vect2);
    double angle (Vecteur vect2);

    void toString();
    double getX();
    double getY();
    double getZ();
    void setX(double x);
    void setY(double y);
    void setZ(double z);

	private:
    double _x,_y,_z;

};
