///////////////////////////////////////////////////////////////////////////////
// Imagina
// ----------------------------------------------------------------------------
// IN - Synthèse d'images - Modélisation géométrique
// Auteur : Gilles Gesquière
// ----------------------------------------------------------------------------
// Base du TP 1
// programme permettant de créer des formes de bases.
// La forme représentée ici est un polygone blanc dessiné sur un fond rouge
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>


/* Dans les salles de TP, vous avez généralement accès aux glut dans C:\Dev. Si ce n'est pas le cas, téléchargez les .h .lib ...
Vous pouvez ensuite y faire référence en spécifiant le chemin dans visual. Vous utiliserez alors #include <glut.h>.
Si vous mettez glut dans le répertoire courant, on aura alors #include "glut.h"
*/

#include "../glut-3.6/include/GL/glut.h"
//#include "./Vecteur.h"
#include "./Point.h"

// Définition de la taille de la fenêtre
#define WIDTH  480
#define HEIGHT 480

// Définition de la couleur de la fenêtre
#define RED   0
#define GREEN 0
#define BLUE  0
#define ALPHA 1


// Touche echap (Esc) permet de sortir du programme
#define KEY_ESC 27


// Entêtes de fonctions
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);

using namespace std;
int main(int argc, char **argv)
{
  // initialisation  des paramètres de GLUT en fonction
  // des arguments sur la ligne de commande
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA);

  // définition et création de la fenêtre graphique, ainsi que son titre
  glutInitWindowSize(WIDTH, HEIGHT);
  glutInitWindowPosition(0, 0);
  glutCreateWindow("TP prog 3D");

  // initialisation de OpenGL et de la scène
  initGL();
  init_scene();

  // choix des procédures de callback pour
  // le tracé graphique
  glutDisplayFunc(&window_display);
  // le redimensionnement de la fenêtre
  glutReshapeFunc(&window_reshape);
  // la gestion des événements clavier
  glutKeyboardFunc(&window_key);

  // la boucle prinicipale de gestion des événements utilisateur
  glutMainLoop();

  return 1;
}

// initialisation du fond de la fenêtre graphique : noir opaque
GLvoid initGL()
{
  glClearColor(RED, GREEN, BLUE, ALPHA);
}

// Initialisation de la scene. Peut servir à stocker des variables de votre programme
// à initialiser
void init_scene()
{
}

// fonction de call-back pour l´affichage dans la fenêtre

GLvoid window_display()
{
  glClear(GL_COLOR_BUFFER_BIT);
  glLoadIdentity();

  // C'est l'endroit où l'on peut dessiner. On peut aussi faire appel
  // à une fonction (render_scene() ici) qui contient les informations
  // que l'on veut dessiner
  render_scene();

  // trace la scène grapnique qui vient juste d'être définie
  glFlush();
}

// fonction de call-back pour le redimensionnement de la fenêtre

GLvoid window_reshape(GLsizei width, GLsizei height)
{
  glViewport(0, 0, width, height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  // ici, vous verrez pendant le cours sur les projections qu'en modifiant les valeurs, il est
  // possible de changer la taille de l'objet dans la fenêtre. Augmentez ces valeurs si l'objet est
  // de trop grosse taille par rapport à la fenêtre.
  glOrtho(-1.0, 4.0, -1.0, 4.0, -1.0, 4.0);

  // toutes les transformations suivantes s´appliquent au modèle de vue
  glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des événements clavier

GLvoid window_key(unsigned char key, int x, int y)
{
  switch (key) {
  case KEY_ESC:
    exit(1);
    break;

  default:
    printf ("La touche %d n´est pas active.\n", key);
    break;
  }
}

void afficheSegment(Point p1,Point p2)
{
    glBegin(GL_LINES);
		glVertex3f(p1.getX(), p1.getY(), p1.getZ());
		glVertex3f(p2.getX(), p2.getY(), p2.getZ());
    glEnd();
}

void afficheVecteur(Point p1,Vecteur v1)
{
    glBegin(GL_LINES);
		glVertex3f(p1.getX(), p1.getY(), p1.getZ());
		glVertex3f(p1.getX()+v1.getX(), p1.getY()+v1.getY(), p1.getZ()+v1.getZ());
    glEnd();
}
void affichePoint(Point p)
{
	glBegin(GL_POINTS);
		glVertex3f(p.getX(), p.getY(), p.getZ());
	glEnd();
}

void drawCurve(Point* tabp,int n)
{
    glBegin(GL_LINE_STRIP);
        for(int i=0;i<n;i++)
            glVertex3f(tabp[i].getX(), tabp[i].getY(), tabp[i].getZ());
    glEnd();
}
void affichePoints(Point* tabp,int n)
{
    glBegin(GL_POINTS);
        for(int i=0;i<n;i++)
            glVertex3f(tabp[i].getX(), tabp[i].getY(), tabp[i].getZ());
    glEnd();
}


double f1(double u)
{
    return 2*u*u*u-3*u*u+1;
}

double f2(double u)
{
    return -2*u*u*u+3*u*u;
}

double f3(double u)
{
    return u*u*u-2*u*u+u;
}

double f4(double u)
{
    return u*u*u-u*u;
}

double factoriel(double i)
{
    if(i==0)
        return 1.0;
    else
        return(i*factoriel(i-1));
}
double P(double k,double i,double u)
{
    if(k==0)
        return ((1-u)+(u));
    else
        return ((1-u)*P(k-1,i,u)+u*P(k-1,i+1,u));
}
double B(double n,double i,double u)
{
    return (factoriel(n)/(factoriel(i)*factoriel(n-i)))*pow(u,i)*pow((1-u),(n-i));
}
void hermiteCubicCurve(Point tabp[],Point p0,Point p1,Vecteur v0,Vecteur v1, int nbu)
{
    for(int i=0;i<nbu+1;i++)
    {
        double u= (double)i/(double)nbu;
        double x = f1(u)*p0.getX()+f2(u)*p1.getX()+f3(u)*v0.getX()+f4(u)*v1.getX();
        double y = f1(u)*p0.getY()+f2(u)*p1.getY()+f3(u)*v0.getY()+f4(u)*v1.getY();
        double z = f1(u)*p0.getZ()+f2(u)*p1.getZ()+f3(u)*v0.getZ()+f4(u)*v1.getZ();
        tabp[i] = *(new Point(x,y,z));
    }
}
void bernstein(Point tabp[], int nbu,Point tabdep[],int n)
{
    for(int i=0;i<nbu+1;i++)
    {
        double u= (double)i/(double)nbu;
        double x = 0;
        double y = 0;
        double z = 0;
        for(int j=0;j<n;j++)
        {
            double t=P(n,j,u);
            x+=t*tabdep[j].getX();
            y+=t*tabdep[j].getY();
            z+=t*tabdep[j].getZ();

        }
        tabp[i] = *(new Point(x,y,z));
    }
}
void Casteljau(Point tabp[], int nbu,Point tabdep[],int n)
{
    for(int i=0;i<n;i++)
        tabp[i]=tabdep[i];
    for(int i=0;i<nbu;i++)
    {
        double u= (double)i/(double)nbu;
        for(int j=0;j<n;j++)
        {
            tabp[j].setX((1-u)*tabp[j].getX()+u*tabdep[j+1].getX());
            tabp[j].setY((1-u)*tabp[j].getY()+u*tabdep[j+1].getY());
            tabp[j].setZ((1-u)*tabp[j].getZ()+u*tabdep[j+1].getZ());
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction que vous allez modifier afin de dessiner
//////////////////////////////////////////////////////////////////////////////////////////
void render_scene()
{
//Définition de la couleur
 glColor3f(1.0, 1.0, 1.0);
  //  Nous créons ici un polygone. Nous pourrions aussi créer un triangle ou des lignes. Voir ci-dessous les parties
  // en commentaires (il faut commenter le bloc qui ne vous intéresse pas et décommenter celui que vous voulez tester.
  // Création de deux lignes
/*	glBegin(GL_LINES);
		glVertex3f(-1, -1, 0);
		glVertex3f(1, 1, 0);
		glVertex3f(1, -1, 0);
		glVertex3f(-1, 1, 0);
	glEnd();
*/
 // création d'un polygone
/*	glBegin(GL_POLYGON);
		glVertex3f(-1, -1, 0);
		glVertex3f(1, -1, 0);
		glVertex3f(1, main.cpp:278:24: error: expected type-specifier before ‘Vector’
1, 0);
		glVertex3f(-1, 1, 0);
	glEnd();
*/



// création d'un triangle
/*	glBegin(GL_TRIANGLES);
		glVertex3f(-1, -1, 0);
		glVertex3f(1, -1, 0);Point.h:14:31: error: expected ‘;’ at end of member declaration

		glVertex3f(1, 1, 0);
	glEnd();
*/

// création d'un tpoint
/*  glPointSize(2);
	glBegin(GL_POINTS);
		glVertex3f(0, 0, 0);
	glEnd();
*/

/*  TP1 PROGRAMMATION 3D */
/*
   glPointSize(2);
    Point* p1 = new Point(2.0,0.0,0.0);
    affichePoint(*p1);
    Point* p2 = new Point(-1.0,-1.0,0.0);
    affichePoint(*p2);
    Point* p3 = new Pomain.cpp:278:24: error: expected type-specifier before ‘Vector’
int(1.0,1.0,0.0);
    affichePoint(*p3);
    Vecteur* v1 = p1->toVecteur(*p2);
    afficheSegment(*p1,*p2);
    Point* p5 = (p3->projectOnLine(*p1,*v1));
    glColor3f(1.0,0,0);
    glPointSize(5);main.cpp:278:24: error: expected type-specifier before ‘Vector’

    affichePoint(*p5);
    afficheSegment(*p5,*p3);
    p5->toString();
*/


/*  TP2 PROGRAMMATION 3D */
/*   //exo 1
    int nbu = 10;
    Point p0 = *(new Point(0.0,0.0,0.0));
    Point p1 = *(new Point(2.0,0.0,0.0));
    Vecteur v0 = *(new Vecteur(1.0,1.0,0.0));
    Vecteur v1 = *(new Vecteur(1.0,-1.0,0.0));
    afficheVecteur(p0,v0);
    afficheVecteur(p1,v1);
    Point tabp[nbu+1] ;
    hermiteCubicCurve(tabp,p0,p1,v0,v1,nbu);
    for(int i=0;i<nbu+1;i++)
    {
        cout<<"i:"<<i<<" x:"<<tabp[i].getX()<<" y:"<<tabp[i].getY()<<endl;
        affichePoint(tabp[i]);
    }
    drawCurve(tabp,nbu+1);
*/
   //exo 2
    int nbu = 20;
    int n=7;
    Point tabdep[n];
    tabdep[0] = *(new Point(0.0,0.0,0.0));
    tabdep[1] = *(new Point(1.0,1.0,0.0));
    tabdep[2] = *(new Point(2.0,1.0,0.0));
    tabdep[3] = *(new Point(2.0,0.0,0.0));
    tabdep[4] = *(new Point(1.0,0.0,0.0));
    tabdep[5] = *(new Point(0.0,2.0,0.0));
    tabdep[6] = *(new Point(1.5,2.2,0.0));
    glPointSize(2);
    affichePoints(tabdep,n);
    Point tabp[nbu+1] ;
    bernstein(tabp,nbu,tabdep,n);
    cout<<1;
    drawCurve(tabp,nbu+1);


    //exo 3

}
