#ifndef INSTANCESAT_H
#define INSTANCESAT_H


class InstanceSAT
{
    public:
        InstanceSAT();
        InstanceSAT(char *filename);
        virtual ~InstanceSAT();
        int getNbVar();
        int getTab(int i,int j);
        int getNbContraite();
        void toString();

    protected:

    private:
    int _nbVar;
    int **_tab;
    int _nbContraite;
};

#endif // INSTANCESAT_H
