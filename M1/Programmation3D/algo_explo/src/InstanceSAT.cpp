#include "../include/InstanceSAT.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
typedef int INT;


InstanceSAT::InstanceSAT()
{
    _nbContraite=0;
    _nbVar=0;
}

InstanceSAT::~InstanceSAT()
{
    //dtor
}

InstanceSAT::InstanceSAT(char *filename)
{
    int l = strlen(filename);
    	if(l <= 3) // Le fichier ne peut pas etre que ".pgm" ou ".ppm"
	{
		printf("Chargement impossible :Le fichier n'est pas conforme");
		exit(0);
	}

    ifstream fichier("test.cnf", ios::in);  // on ouvre le fichier en lecture
    if(fichier)  // si l'ouverture a réussi
    {
        string line;
        getline(fichier, line);
        stringstream(line)>>_nbVar;
        getline(fichier, line);
        stringstream(line)>>_nbContraite;
        (_tab = (INT**) calloc (_nbContraite*3, sizeof(INT) ) );
        for(int i=0;i<_nbContraite;i++)
        {
            getline(fichier, line);
            stringstream(line)>>_tab[i][1]>>_tab[i][2]>>_tab[i][3];
        }
        fichier.close();  // on ferme le fichier
    }
    else  // sinon
            cerr << "Impossible d'ouvrir le fichier !" << endl;
}

int InstanceSAT::getNbVar()
{
    return _nbVar;
}

int InstanceSAT::getTab(int i,int j)
{
    return _tab[i][j];
}


int InstanceSAT::getNbContraite()
{
    return _nbContraite;
}

void InstanceSAT::toString()
{
    cout<<"nbVariable:"<<_nbVar<<endl;
    cout<<"nbContraite:"<<_nbContraite<<endl;
    if((_nbVar!=0) && (_nbContraite!=0))
        for(int i=0;i<_nbContraite;i++)
            cout<<"contraite n°"<<_nbContraite<<"  : "<<_tab[i][1]<<" , "<<_tab[i][2]<<" , "<<_tab[i][1]<<endl;
}
